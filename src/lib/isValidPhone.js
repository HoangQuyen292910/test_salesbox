//@flow

// const phoneRegex = /^[+]?[(]?[0-9]{1,6}[)]?[-\s.]?[0-9]{1,6}[-\s.]?[0-9]{1,6}[-\s.]?[0-9]{1,6}[-\s.]?[0-9]{1,6}$/im;
const phoneRegex = /^[0-9-+\s]+$/
export default (phone: string) => phoneRegex.test(phone);
