import React from 'react';
import CompetencePane from './CompetencePane';
import css from './Competence.css';

const RightContent = (props) => {
  return (
    <div
      className={css.divFixbug0804}
      style={{
        padding: '10px',
      }}
    >
      <CompetencePane {...props} />
    </div>
  );
};

export default RightContent;
