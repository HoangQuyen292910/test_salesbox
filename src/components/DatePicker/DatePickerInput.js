// @flow
import * as React from 'react';
import { Popup, Input } from 'semantic-ui-react';
import { compose, withHandlers, withProps, withState, lifecycle } from 'recompose';
import _l from 'lib/i18n';
import cx from 'classnames';
import DatePicker from './DatePicker';
import css from './DatePicker.css';
import moment from 'moment';
import { forEach, isString } from 'lodash';
import { connect } from 'react-redux';
import 'moment/locale/vi';
import 'moment/locale/de';
import 'moment/locale/sv';
import 'moment/locale/es';
import 'moment/locale/en-ie';
const ENTER_KEY = 13;

type PropsType = {
  value: Date,
  onChange: (Date) => void,
  onMouseDown: (event: Event) => void,
  label: string,
  disabled: boolean,
  size?: string,
};

const toDate = (timePicker, date) => {
  return timePicker && date ? _l`${date}:t(h)` : moment(date).format('DD MMM YYYY');
};

const validate = (dateTime) => {
  const stamp = dateTime.split(' ');
  const validDate = !/Invalid|NaN/.test(new Date(stamp[0]).toString());
  const validTime = /^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/i.test(stamp[1]);
  return !validDate || !validTime || (stamp && stamp.length > 2);
};

const validateDate = (dateTime) => {
  /*
  const stamp = dateTime.split(' ');
  // const validDate = !/Invalid|NaN/.test(new Date(stamp[0]).toString());
  return !validDate || (stamp && stamp.length > 2);
*/
  return false;
};
const monthsShorts = [
  'jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec'.split('_'),
  'Jan._Feb._März_Apr._Mai_Juni_Juli_Aug._Sep._Okt._Nov._Dez.'.split('_'),
  'ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic'.split('_'),
  'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
];
const monthsShortEN = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
const getMonth = (month) => {
  let _text = month;
  forEach(monthsShorts, (list) => {
    forEach(list, (item, index) => {
      if (month === item) {
        _text = monthsShortEN[index];
        return;
      }
    });
  });
  return _text;
};
// const currentLocal = ['sv', 'en', 'de', 'es', 'en-ie']; //'sv', 'en', 'de', 'es', 'en-ie'
const localizationDate = (date) => {
  if (!isString(date)) {
    date = date.toString();
  }
  if (isString(date)) {
    const _str = date?.split(" ");
    const _newDate = date.replace(_str[1], getMonth(_str[1]));
    return _newDate;
  }
}

const DatePickerInput = ({
  handleClick,
  size,
  disabled,
  label,
  value,
  onMouseDown,
  onChange,
  handOnChange,
  inputValue,
  handleKeyDown,
  handleBlur,
  isError,
  handleOnSelectDate,
  open,
  setOpen,
  placeholder,
  currentLocal,
  ...other
}: PropsType) => {
  const input = (
    <Input
      size={size}
      fluid
      icon="calendar"
      value={inputValue}
      onChange={handOnChange}
      onKeyDown={handleKeyDown}
      onClick={() => setOpen(true)}
      onBlur={handleBlur}
      placeholder={placeholder}
      className={isError ? cx(css.dateError, css.inputSize) : css.inputSize}
    />
  );
  const momentDate = inputValue ? moment(inputValue, 'DD MMM YYYY, HH:mm', currentLocal).valueOf() : new Date();
  return (
    <Popup
      onClose={() => setOpen(false)}
      open={open}
      onClick={handleClick}
      onMouseDown={onMouseDown}
      on="click"
      trigger={input}
      flowing
      placeholder=""
    >
      <DatePicker
        selected="null"
        value={momentDate ? momentDate : new Date().getTime()}
        onSelect={handleOnSelectDate}
        {...other}
        placeholder={placeholder}
      />
    </Popup>
  );
};
const mapStateToProps = (state) => {
  return {
    locale: state.ui && state.ui.app && state.ui.app.locale,
  };
};
export default compose(
  connect(mapStateToProps, null),
  withProps(({ timePicker, value, locale }) => {
    let _lang = locale;
    if (_lang === 'se') {
      _lang = 'sv';
    }
    moment.locale(_lang);
    const label =
      timePicker && value
        ? _l`${moment(value).format('DD MMM YYYY, HH:mm')}:t(h)`
        : value ? moment(value).format('DD MMM YYYY') : ''; // label = localizationDate(label)
    return {
      label
    };
  }),
  withState('inputValue', 'setValue', (props) => {
    return props.label;
  }),
  withState('currentLocal', 'setCurrentLocal', (props) => {
    let _lang = props.locale;
    if (_lang === 'se') {
      _lang = 'sv';
    }
    return _lang;
  }),
  withState('format', 'setFormat', ( {timePicker}) => {
    if (timePicker) {
      return 'DD MMM YYYY, HH:mm';
    }
    return 'DD MMM YYYY';
  }),
  withState('open', 'setOpen', false),
  withState('isError', 'setError', false),
  withState('inputDraft', 'setInputDraft', null),
  lifecycle({
    componentWillMount() {
      const { locale } = this.props;
      let _lang = locale;
      if (_lang === 'se') {
        _lang = 'sv';
      }
      moment.locale(_lang);
    },
    componentDidUpdate(prevProps) {
      if (
        prevProps.value !== this.props.value ||
        prevProps.inputDraft !== this.props.inputDraft
        // && prevProps.value
      ) {
        const { value, timePicker, locale } = this.props;
        let _lang = locale;
        if (_lang === 'se') {
          _lang = 'sv';
        }
        moment.locale(_lang);

        let label =
          timePicker && value
            ? _l`${moment(value).format('DD MMM YYYY, HH:mm')}:t(h)`
            : moment(value).format('DD MMM YYYY');
        label = value === null || value === '' ? '' : label;
        this.props.setValue(label);
        // this.props.setValue(localizationDate(label));
      }
    },
  }),
  withHandlers({
    onMouseDown: () => (event) => {
      event.preventDefault();
    },
    handleClick: () => (event) => {
      event.preventDefault();
      event.stopPropagation();
    },
    handOnChange: (props) => (e) => {
      props.setValue(e.target.value);
    },
    handleKeyDown: (props) => (e) => {
      if (e.keyCode === ENTER_KEY) {
        if (props.isValidate) {
          if (props.timePicker ? validate(e.target.value) : validateDate(e.target.value)) {
            props.setError(true);
            const newDate = new Date();
            props.setValue(e.target.value);
            props.setValue(toDate(props.timePicker ? true : false, newDate));
            setTimeout(() => {
              props.setError(false);
            }, 2000);
          } else {
            props.setError(false);
            props.setValue(e.target.value);

            const newValue = new Date(e.target.value);
            props.onChange(newValue);
            props.setOpen(false);
          }
        }
      }
    },
    handleBlur: (props) => (e) => {
      // moment.locale('en');
      props.setInputDraft(e.target.value);
      if (props.isValidate) {
        if (props.timePicker ? validate(e.target.value) : validateDate(e.target.value)) {
          props.setError(true);
          const newDate = new Date();
          props.setValue(e.target.value);
          props.setValue(toDate(props.timePicker ? true : false, newDate));
          setTimeout(() => {
            props.setError(false);
          }, 2000);
        } else {
          props.setError(false);
          props.setValue(e.target.value);
          let newValue = e.target.value != null && e.target.value != '' ? moment(e.target.value, props.format, props.currentLocal).valueOf() : null;
          newValue = newValue === 'Invalid Date' ? new Date() : newValue;
          // if (props.handlePropsEvent) {
          //   return props.handlePropsEvent(newValue);
          // }
          props.setOpen(false);
          props.onChange(newValue);
        }
      } else {
        try {
          props.setError(false);
          props.setValue(e.target.value);

          let newValue = e.target.value != null && e.target.value != '' ? moment(e.target.value, props.format, props.currentLocal).valueOf() : null;
          newValue = newValue === 'Invalid Date' ? new Date() : newValue;
          // if (props.handlePropsEvent) {
          //   return props.handlePropsEvent(newValue);
          // }
          props.onChange(newValue);
          props.setOpen(false);
        } catch(ex) {
          console.log("🚀 ~ file: DatePickerInput.js ~ line 240 ~ ex", ex)
        }
      }
    },
    handleOnSelectDate: ({ setOpen, onChange, setValue, timePicker }) => (valueDate) => {
      const label =
        timePicker && valueDate
          ? _l`${moment(valueDate).format('DD MMM YYYY, HH:mm')}:t(h)`
          : moment(valueDate).format('DD MMM YYYY');
      setValue(label);
      console.log("=================>", valueDate)
      onChange(new Date(valueDate).getTime());
      if (timePicker) {
        setOpen(true);
      } else {
        setOpen(false);
      }
    },
  })
)(DatePickerInput);
