import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import {
  Button,
  Dimmer,
  Divider,
  Grid,
  GridColumn,
  GridRow,
  Icon,
  Input,
  Loader,
  Table,
  Dropdown,
} from 'semantic-ui-react';
import addIcon from '../../../public/Add.svg';
import style from './Signature.css';
import _l from 'lib/i18n';
import { withRouter } from 'react-router';
import cx from 'classnames';
import { selectMode, initDataTemplateSelected } from './signature.actions';
import { TemplateDropdown } from './TemplateDropdown';
import ModalCommon from '../ModalCommon/ModalCommon';
import api from 'lib/apiClient';
import { BLOCKTYPE_SIGNATURE, Endpoints, ObjectTypes } from '../../Constants';
import * as NotificationActions from '../Notification/notification.actions';
import moment from 'moment';
import ModalSendEmailAddDeal from '../Resources/ResourceDetail/ModalSendEmailAddDeal';
import {
  userTags,
  dealTags,
  contactTags,
  companyTags,
  specialTags,
  companyCf,
  contactCf,
  dealCf,
  trans,
  dateDeal,
  tagMessages, ownerName, dayInPipe
} from './BlockElements/tagMessages';

export const Header = (props) => {
  const {
    MODE,
    selectMode,
    handleExport,
    objectType,
    notiSuccess,
    enterpriseId,
    signatureStore,
    listCompanies,
    initDataTemplateSelected,
    eSignOption,
    setEsignOption,
    sendForSigning,
    showModelESign,
    setShowModelESign,
    putError,
    chosenParties,
    fromParties,
    userID,
    isPDF,
    isSendingForSigner,
    attachmentsBlock,
    getPdfToSend,
    fileAttachSendMail,
    isShowMassPersonalEmail,
    showHideMassPersonalMail,
    accountAllowedSendEmail,
    __COMPANY,
    __CONTACT,
    __USER_LOGIN_INFO,
    __DEAL, dataCFCompany, dataCFContact, dataCFDeal
  } = props;

  const [templateSelected, setTemplateSelected] = useState(null);
  const [loading, setLoading] = useState(false);
  const [showModalDeleteDocument, setShowModelDeleteDocument] = useState(false);

  const onClickIconClose = () => {
    props.history.goBack();
  };

  const [nameTemplate, setNameTemplate] = useState(null);
  const [errorName, setErrorName] = useState(null);

  const handleDataBeforeSaveTemplate = (signature) => {
    let indexAttachmentBlock = signature.__ELEMENTS?.findIndex((e) => e.type === BLOCKTYPE_SIGNATURE.ATTACHMENT);
    let newElements = [...signature.__ELEMENTS];
    if (indexAttachmentBlock != -1) {
      newElements[indexAttachmentBlock] = {
        type: BLOCKTYPE_SIGNATURE.ATTACHMENT,
        attachments: [],
        attachmentsUrl: attachmentsBlock?.attachmentsUrl,
      };
    }
    newElements.forEach((i, index) => {
      if (i.type === BLOCKTYPE_SIGNATURE.TEXT) {
        newElements[index] = {
          ...i,
          contentTemp: i.content,
        };
      }
      if (i.type === BLOCKTYPE_SIGNATURE.COVER) {
        newElements[index] = {
          ...i,
          contentTemp: i.content,
        };
      }
      if (i.type === BLOCKTYPE_SIGNATURE.HEADER) {
        newElements[index] = {
          ...i,
          contentTemp: i.content,
        };
      }
    });
    let data = {
      __ELEMENTS: newElements,
      __SETTING_COLUMN_PRODUCT: signature.__SETTING_COLUMN_PRODUCT,
    };
    return JSON.stringify(data);
  };

  const [isSavingTemplate, setIsSavingTemplate] = useState(false);
  const [isSavingVersion, setIsSavingVersion] = useState(false);

  const onDoneSaveTemplate = async () => {
    if (!nameTemplate || nameTemplate?.trim() === '') {
      setErrorName(_l`Name is required`);
      setIsSavingTemplate(false);
      return;
    }
    let dataToSave = handleDataBeforeSaveTemplate(signatureStore);
    if (!templateSelected || originalName !== nameTemplate) {
      try {
        const res = await api.post({
          resource: `${Endpoints.Enterprise}/e-signature/addDocumentTemplate`,
          query: {
            objectType: objectType || localStorage.getItem('objectTypeSignature'),
          },
          data: {
            enterpriseId,
            name: nameTemplate,
            htmlTemplate: dataToSave,
            // uuid: templateSelected,
          },
        });
        if (res) {
          notiSuccess(`Success`, '', 2000);
          setshowModalSaveDocument(false);
          setIsSavingTemplate(false);
          setErrorName(null);
          setNameTemplate(null);
          setVersionName(null);
          setVersionSelected(null);
          // setTemplates([...templates, { key: res.uuid, text: res.name, value: res.uuid }]);
          getListTemplate(objectType);
          setTemplateSelected(res.uuid);
        }
      } catch (error) {
        setIsSavingTemplate(false);
        console.log(error);
        switch(error.message) {
          case 'EXISTED':
            return putError('Name already exist.')

        }
      }
    } else {
      try {
        const res = await api.post({
          resource: `${Endpoints.Enterprise}/e-signature/updateDocumentTemplate`,
          data: {
            htmlTemplate: dataToSave,
            uuid: templateSelected,
          },
        });
        if (res) {
          notiSuccess(`Success`, '', 2000);
          setshowModalSaveDocument(false);
          setIsSavingTemplate(false);
          setErrorName(null);
          setNameTemplate(null);
          setVersionName(null);
          setVersionSelected(null);
          // setTemplates([...templates, { key: res.uuid, text: res.name, value: res.uuid }]);
          getListTemplate(objectType);
          setTemplateSelected(res.uuid);
        }
      } catch (error) {
        setIsSavingTemplate(false);
      }
    }
  };
  const [showModalSaveDocument, setshowModalSaveDocument] = useState(false);
  const [showModalSaveVersion, setshowModalSaveVersion] = useState(false);
  // const [showModelESign, setShowModelESign] = useState(false);
  useEffect(() => {
    selectMode('BUILD');
    return () => {
      selectMode('BUILD');
    };
  }, []);

  const handleMethodsSendForSigning = (party, indexInput, method) => {
    if (method !== 'sms' || (method === 'sms' && party.phone)) {
      let remove = isWarning.filter((i) => i !== indexInput);
      setIsWarning(remove);
    }
    let newData = eSignOption;
    if (newData.length === 0) {
      newData.push({
        givenName: party.givenName,
        familyName: party.familyName,
        email: party?.email,
        signingMethod: method,
        language: party.language,
        phone: party?.phone,
      });
      console.log(newData);
      setEsignOption(newData);
    } else {
      let checkIfNew = true;
      newData.forEach((i, index) => {
        if (index === indexInput) {
          i.signingMethod = method;
          checkIfNew = false;
        }
      });
      if (checkIfNew) {
        newData.push({
          givenName: party.givenName,
          familyName: party.familyName,
          email: party?.email,
          signingMethod: method,
          language: party.language,
          phone: party?.phone,
        });
      }
      setEsignOption(newData);
      console.log(eSignOption);
    }
  };
  const handleLanguageSendForSigning = (party, indexInput, language) => {
    let newData = eSignOption;
    if (newData.length === 0) {
      newData.push({
        givenName: party.givenName,
        familyName: party.familyName,
        language: language,
        email: party?.email,
        phone: party?.phone,
      });
      setEsignOption(newData);
    } else {
      let checkIfNew = true;
      newData.forEach((i, index) => {
        if (index === indexInput) {
          i.language = language;
          checkIfNew = false;
        }
      });
      if (checkIfNew) {
        newData.push({
          givenName: party.givenName,
          familyName: party.familyName,
          language: language,
          email: party?.email,
          phone: party?.phone,
        });
      }
      setEsignOption(newData);
    }
  };
  const eIdMethods = [
    { key: 'email', value: 'email', text: 'EMAIL' },
    { key: 'sms', value: 'sms', text: _l`SMS` },
    { key: 'touch-sign', value: 'touch-sign', text: _l`Touch sign` },
    { key: 'bankid-se', value: 'bankid-se', text: _l`BankID Sweden` },
    { key: 'bankid-no', value: 'bankid-no', text: _l`BankID Norway` },
    { key: 'bankid-dk', value: 'bankid-dk', text: _l`BankID Denmark` },
    { key: 'nets-esign-no', value: 'nets-esign-no', text: _l`Nets eSign Norway` },
    { key: 'nets-esign-se', value: 'nets-esign-se', text: _l`Nets eSign Sweden` },
    { key: 'nets-esign-dk', value: 'nets-esign-dk', text: _l`BankID FI Nets e-ident` },
    { key: 'nets-esign-tupas', value: 'nets-esign-tupas', text: _l`Nets eSign Tupas` },
    { key: 'nets-eident-fi', value: 'nets-eident-fi', text: _l`Nets eIDent Finland` },
    { key: 'nets-eident-mobiilivarmenne', value: 'nets-eident-mobiilivarmenne', text: _l`Nets eIDent Mobilivarmenne` },
    { key: 'nets-eident-dk', value: 'nets-eident-dk', text: _l`Nets eIDent Denmark` },
    { key: 'eideasy-ee-id', value: 'eideasy-ee-id', text: _l`EIDEasy Estonia ID` },
    { key: 'eideasy-lv-id', value: 'eideasy-lv-id', text: _l`EIDEasy Latvia ID` },
    { key: 'eideasy-lt-id', value: 'eideasy-lt-id', text: _l`EIDEasy Lithuania ID` },
    { key: 'eideasy-be-id', value: 'eideasy-be-id', text: _l`EIDEasy Belgium ID` },
  ];
  const languageOption = [
    { key: 'se', value: 'sweden', flag: 'se', text: _l`Swedish` },
    { key: 'us', value: 'england', flag: 'us', text: _l`English` },
  ];
  const saveDocumentAsTemplate = () => {
    let item = templates.find((e) => e.value === templateSelected);
    if (item) {
      setNameTemplate(item.text);
      setOriginalName(item.text);
    }
    setshowModalSaveDocument(true);
  };

  const [currentSignatures, setCurrentSignature] = useState(0);
  const [listAllCompanies, setListAllCompanies] = useState([]);
  const [originalName, setOriginalName] = useState(null);
  const [isWarning, setIsWarning] = useState([]);

  useEffect(() => {
    getCompaniesDTO();
  }, [fromParties]);
  useEffect(() => {
    getCompaniesDTO();
  }, [chosenParties]);

  const getCompaniesDTO = () => {
    let newData = [];
    chosenParties?.forEach((i) => {
      let obj = {
        givenName: i?.firstName?.value,
        familyName: i?.lastName?.value,
        email: i?.email?.value,
        signingMethod: 'email',
        language: 'england',
        phone: i?.phone?.value,
      };
      newData.push(obj);
    });
    if (fromParties) {
      let obj = {
        givenName: fromParties?.firstName?.value,
        familyName: fromParties?.lastName?.value,
        email: fromParties?.email?.value,
        language: 'england',
        signingMethod: 'email',
        phone: fromParties?.phone?.value,
      };
      newData.push(obj);
    }
    console.log('get new data', newData);
    setListAllCompanies(newData);
    setEsignOption(newData);
  };
  const getCurrentQuantitySignatures = async () => {
    let checkMissing = false;
    let checkHaveElement = true;
    if (!signatureStore?.__ELEMENTS || (signatureStore?.__ELEMENTS?.length === 0 && !isPDF)) {
      putError(`You have to design your own document templates before signing`);
      checkMissing = true;
      checkHaveElement = false;
      return;
      // setShowModelESign(false);
    }
    if (
      (!signatureStore?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER) &&
        signatureStore?.__ELEMENTS &&
        checkHaveElement) ||
      eSignOption.length === 0
    ) {
      putError(`Please add parties before Signing`);
      checkMissing = true;
      return;
    }
    let missingAtt = false;
    console.log(eSignOption);
    eSignOption.forEach((i) => {
      if (!i.givenName || !i.familyName || !i.email) {
        checkMissing = true;
        missingAtt = true;
      }
    });
    if (missingAtt) {
      putError(`One of parties was missing email or contact name, please take a look`);
      return;
    }
    if (MODE !== 'PREVIEW') {
      putError(`Change to preview mode before signing`);
      checkMissing = true;
      return;
    }
    if (checkMissing) {
      setShowModelESign(false);
    } else setShowModelESign(true);

    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/payment/getNumberSignature`,
      });
      if (res === 0 && moment(new Date()).isAfter('2021-07-31')) {
        putError('Please subscribe to the E-signatures to use this feature');
        return;
      }
      setCurrentSignature(res);
    } catch (error) {
      console.log(error);
    }
  };
  const getTemplateByUuid = async (uuid) => {
    try {
      setLoading(true);
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/e-signature/getDocumentTemplate/${uuid}`,
      });
      if (res) {
        let data = JSON.parse(res.htmlTemplate);
        if (data?.__ELEMENTS) {
          let partiesBlockIndex = data.__ELEMENTS.findIndex((e) => e.type === BLOCKTYPE_SIGNATURE.PARTER);
          if (partiesBlockIndex !== -1) {
            data.__ELEMENTS[partiesBlockIndex] = {
              type: BLOCKTYPE_SIGNATURE.PARTER,
              __COMPANY_CONTACT_INFO: [
                {
                  companyName: { show: true, value: signatureStore.__COMPANY?.name || '' },
                  vatNumber: { show: true, value: signatureStore.__COMPANY?.vatNumber || '' },
                  address: { show: true, value: signatureStore.__COMPANY?.fullAddress || '' },
                  email: { show: true, value: signatureStore.__CONTACT?.email || '' },
                  phone: { show: true, value: signatureStore.__CONTACT?.phone || '' },
                  title: { show: true, value: signatureStore.__CONTACT?.title || '' },
                  firstName: { show: true, value: signatureStore.__CONTACT?.firstName || '' },
                  lastName: { show: true, value: signatureStore.__CONTACT?.lastName || '' },
                  fullName: {
                    show: true,
                    value:
                      `${signatureStore.__CONTACT?.firstName || ''} ${signatureStore.__CONTACT?.lastName || ''}` || '',
                  },
                  country: {
                    show: true,
                    value: signatureStore.__CONTACT?.country || signatureStore.__COMPANY?.country || null,
                  },
                },
              ],
              __USER_INFO: [
                {
                  companyName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.companyName || '' },
                  vatNumber: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.vatNumber || '' },
                  address: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.address || '' },
                  fullName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.fullName || '' },
                  email: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.email || '' },
                  phone: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.phone || '' },
                  title: { show: true, value: '' },
                  firstName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.firstName || '' },
                  lastName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.lastName || '' },
                  country: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.country || null },
                  fullName: {
                    show: true,
                    value:
                      `${signatureStore?.__USER_LOGIN_INFO?.firstName || ''} ${signatureStore?.__USER_LOGIN_INFO
                        ?.lastName || ''}` || '',
                  },
                },
              ],
            };
          }
          data.__ELEMENTS?.forEach((i) => {
            if (i.type === BLOCKTYPE_SIGNATURE.TEXT) {
              let contentToCheck = i.content
              i.content = convertBackToLanguage(i.content);
              if(i.contentTemp){
                i.contentTemp = convertBackToLanguage(i?.contentTemp)
              } else {
                i.contentTemp = convertBackToLanguage(contentToCheck)
              }
              i?.valuePdf?.ops?.map(x => x.insert = convertToValue(x?.insert))
            }
            if (i.type === BLOCKTYPE_SIGNATURE.COVER) {
              let contentToCheck = i.content
              i.content = convertBackToLanguage(i.content);
              if(i.contentTemp){
                i.contentTemp = convertBackToLanguage(i?.contentTemp)
              } else {
                i.contentTemp = convertBackToLanguage(contentToCheck)
              }
              i?.valuePdf?.ops?.map(x => x.insert = convertToValue(x?.insert))
            }
            if (i.type === BLOCKTYPE_SIGNATURE.HEADER) {
              let contentToCheck = i.content
              i.content = convertBackToLanguage(i.content);
              if(i.contentTemp){
                i.contentTemp = convertBackToLanguage(i?.contentTemp)
              } else {
                i.contentTemp = convertBackToLanguage(contentToCheck)
              }
              i?.contentPdf?.ops?.map(x => x.insert = convertToValue(x?.insert))
            }
          });
        }
        initDataTemplateSelected(data);
        setVersionName(null);
        setVersionSelected(null);
        setLoading(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const convertBackToLanguage = (text) => {
    let matches = text?.match(/\{{(.*?)\}}/g);
    console.log("matches", matches)
    matches?.forEach((e) => {
      if (companyTags.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text?.replace(e.slice(2, e.indexOf('_')), _l`Company`.toString());
        let checkIndex = e.slice(e.indexOf('_') + 1, e.length - 2);
        let getOrigin = trans[checkIndex];
        // let transReplace = _l`${getOrigin}`;
        let transReplace = _l.call(this, [getOrigin])
        text = text?.replace(
          checkIndex,
          transReplace
            .toLowerCase()
            .replaceAll('- ', '')
            .replaceAll(' ', '_')
        );
      }
      if (contactTags.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text?.replace(e.slice(2, e.indexOf('_')), _l`Contact`.toString());
        let checkIndex = e.slice(e.indexOf('_') + 1, e.length - 2);
        let getOrigin = trans[checkIndex];
        let transReplace = _l.call(this, [getOrigin])
        text = text?.replace(
          checkIndex,
          transReplace
            .toLowerCase()
            .replaceAll('- ', '')
            .replaceAll(' ', '_')
        );
      }
      if (dealTags.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text?.replace(e.slice(2, e.indexOf('_')), _l`Deal`.toString());
        let checkIndex = e.slice(e.indexOf('_') + 1, e.length - 2);
        let getOrigin = trans[checkIndex];
        let transReplace = _l.call(this, [getOrigin])
        if (Object.keys(dateDeal).some((t) => checkIndex.includes(t))) {
          text = text?.replace(
            checkIndex,
            _l`Date`.toString().toLowerCase() +
            '_' +
            transReplace
              .toLowerCase()
              .replaceAll('- ', '')
              .replaceAll(' ', '_')
          );
        } else
          text = text?.replace(
            checkIndex,
            transReplace
              .toString()
              .toLowerCase()
              .replaceAll('- ', '')
              .replaceAll(' ', '_')
          );
      }
      if (companyCf.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text?.replace(
          e.slice(2, e.indexOf('_')),
          _l`Company`.toString().toLowerCase() + _l`Custom`.toString()
        );
      }
      if (contactCf.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text?.replace(
          e.slice(2, e.indexOf('_')),
          _l`Contact`.toString().toLowerCase() + _l`Custom`.toString()
        );
      }
      if (dealCf.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text.replace(e.slice(2, e.indexOf('_')), _l`Deal`.toString().toLowerCase() + _l`Custom`.toString());
      }
      if (userTags.some((t) => e.slice(0, e.indexOf('_') + 1).includes(t))) {
        text = text?.replace(e.slice(2, e.indexOf('_')), _l`User`.toString());
        let checkIndex = e.slice(e.indexOf('_') + 1, e.length - 2);
        let getOrigin = trans[checkIndex];
        let transReplace = _l.call(this, [getOrigin])
        text = text?.replace(
          checkIndex,
          transReplace
            .toString()
            .toLowerCase()
            .replaceAll('- ', '')
            .replaceAll(' ', '_')
        );
      }
    });
    return text;
  };

  const [idOfObject, setIdOfObject] = useState(null);

  useEffect(() => {
    getListTemplate(objectType);
  }, [objectType]);

  useEffect(() => {
    let realObj = localStorage.getItem('objectTypeSignature');
    if (realObj === ObjectTypes.Opportunity) {
      setIdOfObject(signatureStore?.__DEAL?.uuid);
    } else if (realObj === ObjectTypes.Contact) {
      setIdOfObject(signatureStore?.__CONTACT?.uuid);
    } else if (realObj === ObjectTypes.Account) {
      setIdOfObject(signatureStore?.__COMPANY?.uuid);
    }
    getAllVersion();
  }, [signatureStore?.__DEAL?.uuid, signatureStore?.__CONTACT?.uuid, signatureStore?.__COMPANY?.uuid]);

  const [templates, setTemplates] = useState([]);
  const [templateUuidToDelete, setTemplateUuidToDelete] = useState(null)
  const getListTemplate = async (objectType) => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/e-signature/getAllTemplates`,
        query: {
          objectType: objectType || localStorage.getItem('objectTypeSignature'),
        },
      });
      if (res?.documentTemplateLiteDTOS) {
        setTemplates(
          res.documentTemplateLiteDTOS.map((e) => {
            // return { key: e.uuid, text: e.name, value: e.uuid, icon: { name: 'remove', onClick: () => console.log('iconClick')}};
            return {
              key: e.uuid,
              text: e.name,
              value: e.uuid,
              description: (
                <Icon
                  className="right floated"
                  name="remove"
                  onClick={(event) => {
                    event.stopPropagation();
                    setTemplateUuidToDelete(e.uuid)
                    setShowModelDeleteDocument(true);
                  }}
                />
              ),
            };
          })
        );
      }
    } catch (error) {
      console.log('=========>>>>>>', error);
    }
  };

  const deleteDocument = async (objId) => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/e-signature/deleteDocumentTemplate/${objId}`,
      });
      if (res) setShowModelDeleteDocument(false);
      let temp = {
        __ELEMENTS: [],
        __SETTING_COLUMN_PRODUCT: [],
      };
      if (templateUuidToDelete === templateSelected) {
        setNameTemplate(null);
        setTemplateSelected(null);
        initDataTemplateSelected(temp);
      }
      setTemplateUuidToDelete(null)
      getListTemplate(objectType || localStorage.getItem('objectTypeSignature'));
    } catch (error) { }
  };

  const [versionSelected, setVersionSelected] = useState(null);
  const [originVersionName, setOriginVersionName] = useState(null);
  const [versionName, setVersionName] = useState(null);
  const [errorVersionName, setErrorVersionName] = useState(null);
  const [versionList, setVersionList] = useState([]);

  const handleCopyVersion = async () => {
    let dataToSave = handleDataBeforeSaveTemplate(signatureStore);
    if (!versionSelected || originVersionName !== versionName) {
      try {
        const res = await api.post({
          resource: `${Endpoints.Enterprise}/e-signature/documentSavedCopy/add`,
          data: {
            objectType: objectType || localStorage.getItem('objectTypeSignature'),
            name: versionName,
            savedDocument: dataToSave,
            objectId: idOfObject,
          },
        });
        if (res) setIsSavingVersion(false);
        setshowModalSaveVersion(false);
        getAllVersion();
        setVersionName(res.name);
        setVersionSelected(res.uuid);
        setNameTemplate(null);
        setTemplateSelected(null);
      } catch (error) {
        setIsSavingVersion(false);
        console.log(error);
        switch(error.message) {
          case 'EXISTED':
            return putError('Name already exist.')

        }
      }
    } else {
      try {
        const res = await api.post({
          resource: `${Endpoints.Enterprise}/e-signature/documentSavedCopy/update`,
          data: {
            savedDocument: dataToSave,
            uuid: versionSelected,
          },
        });
        if (res) setIsSavingVersion(false);
        setshowModalSaveVersion(false);
        getAllVersion();
        setNameTemplate(null);
        setTemplateSelected(null);
      } catch (error) { }
    }
  };
  const deleteVersion = async (objId) => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/e-signature/documentSavedCopy/delete/${objId}`,
      });
      if (res) setShowModelDeleteVersion(false);
      let temp = {
        __ELEMENTS: [],
        __SETTING_COLUMN_PRODUCT: [],
      };
      if (versionToDelete === versionSelected) {
        setVersionName(null);
        setVersionSelected(null);
        setVersionSelected(null)
        initDataTemplateSelected(temp);
      }
      setVersionToDelete(null)
      getAllVersion();
    } catch (error) { }
  };

  const [versionToDelete, setVersionToDelete] = useState(null)

  const getAllVersion = async () => {
    let obj =
      signatureStore?.objId || localStorage.getItem('objectTypeSignature') === ObjectTypes.Opportunity
        ? signatureStore?.__DEAL?.uuid
        : localStorage.getItem('objectTypeSignature') === ObjectTypes.Contact
          ? signatureStore?.__CONTACT?.uuid
          : signatureStore?.__COMPANY?.uuid;
    console.log('obj is', idOfObject, 'with ', localStorage.getItem('objectTypeSignature'));
    if(idOfObject){
      try {
        const res = await api.get({
          resource: `${Endpoints.Enterprise}/e-signature/documentSavedCopy/getAllByObjectId`,
          query: {
            objectId: idOfObject,
          },
        });
        if (res)
          setVersionList(
            res?.documentSavedCopyDTOS?.map((e) => {
              return {
                key: e?.uuid,
                value: e?.uuid,
                text: e?.name,
                description: (
                  <Icon
                    className="right floated"
                    name="remove"
                    onClick={(event) => {
                      event.stopPropagation()
                      setVersionToDelete(e?.uuid)
                      setShowModelDeleteVersion(true);
                    }}
                  />
                ),
              };
            })
          );
      } catch (error) { }
    }
  };
  const [showDeleteVersionModal, setShowModelDeleteVersion] = useState(false);
  const getVersionSelected = async (objId) => {
    try {
      setLoading(true);
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/e-signature/documentSavedCopy/get/${objId}`,
      });
      if (res) {
        let data = JSON.parse(res.savedDocument);
        if (data?.__ELEMENTS) {
          let partiesBlockIndex = data.__ELEMENTS.findIndex((e) => e.type === BLOCKTYPE_SIGNATURE.PARTER);
          if (partiesBlockIndex !== -1) {
            data.__ELEMENTS[partiesBlockIndex] = {
              type: BLOCKTYPE_SIGNATURE.PARTER,
              __COMPANY_CONTACT_INFO: [
                {
                  companyName: { show: true, value: signatureStore.__COMPANY?.name || '' },
                  vatNumber: { show: true, value: signatureStore.__COMPANY?.vatNumber || '' },
                  address: { show: true, value: signatureStore.__COMPANY?.fullAddress || '' },
                  email: { show: true, value: signatureStore.__CONTACT?.email || '' },
                  phone: { show: true, value: signatureStore.__CONTACT?.phone || '' },
                  title: { show: true, value: signatureStore.__CONTACT?.title || '' },
                  firstName: { show: true, value: signatureStore.__CONTACT?.firstName || '' },
                  lastName: { show: true, value: signatureStore.__CONTACT?.lastName || '' },
                  fullName: {
                    show: true,
                    value:
                      `${signatureStore.__CONTACT?.firstName || ''} ${signatureStore.__CONTACT?.lastName || ''}` || '',
                  },
                  country: {
                    show: true,
                    value: signatureStore.__CONTACT?.country || signatureStore.__COMPANY?.country || null,
                  },
                },
              ],
              __USER_INFO: [
                {
                  companyName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.companyName || '' },
                  vatNumber: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.vatNumber || '' },
                  address: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.address || '' },
                  fullName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.fullName || '' },
                  email: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.email || '' },
                  phone: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.phone || '' },
                  title: { show: true, value: '' },
                  firstName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.firstName || '' },
                  lastName: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.lastName || '' },
                  country: { show: true, value: signatureStore?.__USER_LOGIN_INFO?.country || null },
                  fullName: {
                    show: true,
                    value:
                      `${signatureStore?.__USER_LOGIN_INFO?.firstName || ''} ${signatureStore?.__USER_LOGIN_INFO
                        ?.lastName || ''}` || '',
                  },
                },
              ],
            };
          }
          data?.__ELEMENTS?.forEach((i) => {
            if (i.type === BLOCKTYPE_SIGNATURE.TEXT) {
              let contentToCheck = i.content
              i.content = convertBackToLanguage(i.content);
              i.contentTemp = i.contentTemp ? convertBackToLanguage(i.contentTemp) : convertBackToLanguage(contentToCheck);
              i?.valuePdf?.ops?.map(x => x.insert = convertToValue(x?.insert))
            }
            if (i.type === BLOCKTYPE_SIGNATURE.COVER) {
              let contentToCheck = i.content
              i.content = convertBackToLanguage(i.content);
              i.contentTemp = i.contentTemp ? convertBackToLanguage(i.contentTemp) : convertBackToLanguage(contentToCheck);
              i?.valuePdf?.ops?.map(x => x.insert = convertToValue(x?.insert))
            }
            if (i.type === BLOCKTYPE_SIGNATURE.HEADER) {
              let contentToCheck = i.content
              i.content = convertBackToLanguage(i.content);
              i.contentTemp = i.contentTemp ? convertBackToLanguage(i.contentTemp) : convertBackToLanguage(contentToCheck);
            }
          });
        }
        //  else {
        //   data['__ELEMENTS'] = [];
        // }
        initDataTemplateSelected(data);
        setVersionName(res?.name);
        setOriginVersionName(res?.name);
        setLoading(false);
        setNameTemplate(null);
        setTemplateSelected(null);
      }
      setLoading(false);
    } catch (error) { }
  };

  const convertToValue = ( text ) => {
    let matches = text.match(/\{{(.*?)\}}/g)
    let changeContent = text
    matches?.forEach(i => {
      if (companyTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (specialTags[checkIndex]) {
          let key = __COMPANY?.[specialTags[checkIndex]]?.name
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          let key = __COMPANY?.[tagMessages[checkIndex]]
          changeContent = changeContent.replaceAll(i, key ? key : '')
        }
      }
      if (contactTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (specialTags[checkIndex]) {
          let key = __CONTACT?.[specialTags[checkIndex]]?.name
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          let key = __CONTACT?.[tagMessages[checkIndex]]
          changeContent = changeContent.replaceAll(i, key ? key : '')
        }
      }
      if (dealTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (Object.keys(ownerName).some(t => checkIndex.includes(t))) {
          let key = __DEAL?.participantList?.[0][ownerName[checkIndex]]?.toString()
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          // let key = __DEAL?.[checkIndex]
          if (Object.keys(dayInPipe).some(t => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(i, __DEAL[dayInPipe[checkIndex]] ? Math.ceil(__DEAL[dayInPipe[checkIndex]] / 1000 / 3600 / 24)?.toString() : '')
          }
          if (Object.keys(dateDeal).some(t => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(i, __DEAL[dateDeal[checkIndex]] ? new moment(__DEAL[dateDeal[checkIndex]]).format('DD-MM-YYYY')?.toString() : '')
          } else {
            changeContent = changeContent.replaceAll(i, __DEAL[tagMessages[checkIndex]] ? __DEAL[tagMessages[checkIndex]] : '')
          }
        }
      }
      if (userTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        let key = __USER_LOGIN_INFO?.[tagMessages[checkIndex]]
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
      if (companyCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFCompany?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value
        if (value?.customFieldValueDTOList?.[0]?.value) {
          changeContent = changeContent.replace(i, key)
        } else {
          changeContent = changeContent.replaceAll(i, '')
        }
      }
      if (contactCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFContact?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value?.toString()
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
      if (dealCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFDeal?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
    })
    return changeContent
  }
  return (
    <>
      {loading && (
        <Dimmer active>
          <Loader active={loading} />
        </Dimmer>
      )}
      <Grid>
        <GridRow>
          <GridColumn width={3}>
            <div className={style.divCloseIcon} onClick={onClickIconClose}>
              <img src={addIcon} className={style.closeIcon} />
            </div>
          </GridColumn>
          <GridColumn width={2} className={style.dflex}>
            {!props.isPDF ? (
              <>
                <TemplateDropdown
                  // style={{ width: '50%' }}
                  objectType={objectType}
                  templates={templates}
                  value={templateSelected}
                  onChange={(e, { value }) => {
                    getTemplateByUuid(value);
                    setTemplateSelected(value);
                  }}
                />
              </>
            ) : (
              <div style={{ width: '200' }}></div>
            )}
          </GridColumn>
          <GridColumn width={2} className={style.dflex}>
          {!props.isPDF ? (
            <Dropdown
              // style={{ width: '50%' }}
              noResultsMessage={_l`No results found`}
              lazyLoad
              selection
              options={versionList}
              search
              placeholder={_l`Choose copy`}
              onChange={(e, { value }) => {
                getVersionSelected(value);
                setVersionSelected(value);
              }}
              value={versionSelected}
            ></Dropdown>
            ) : (
              <div style={{ width: '200' }}></div>
            )}
          </GridColumn>
          <GridColumn width={3} className={style.dflex}>
            <Button.Group style={{ position: 'relative', margin: 'auto' }}>
              <Button
                onClick={() => selectMode('BUILD')}
                style={{ borderTopRightRadius: '0 !important', borderBottomRightRadius: '0 !important' }}
                className={cx(style.btnRadio, MODE === 'BUILD' && style.btnDeActive)}
              >
                {_l`Edit`}
              </Button>
              <Button
                onClick={() => selectMode('PREVIEW')}
                style={{ borderTopLeftRadius: '0 !important', borderBottomLeftRadius: '0 !important' }}
                className={cx(style.btnRadio, MODE === 'PREVIEW' && style.btnDeActive)}
              >
                {_l`Preview`}
              </Button>
              {/* {MODE === 'PREVIEW' && (
                <Button.Group style={{ position: 'absolute', left: 155 }}>
                  <Button style={{ height: 28, padding: '2 8' }} icon={<Icon name="desktop" />}></Button>
                  <Button style={{ height: 28, padding: '2 8' }} icon={<Icon name="tablet alternate" />}></Button>
                  <Button style={{ height: 28, padding: '2 8' }} icon={<Icon name="mobile alternate" />}></Button>
                </Button.Group>
              )} */}
            </Button.Group>
          </GridColumn>
          <GridColumn width={3} className={style.dflex}>
          <Button
              onClick={saveDocumentAsTemplate}
              className={style.btn}
              style={{ width: '50%', visibility: props.isPDF ? 'hidden' : 'visible' }}
            >{_l`Save as template`}</Button>
            <Button
              onClick={() => setshowModalSaveVersion(true)}
              className={style.btn}
              style={{ width: '50%', visibility: props.isPDF ? 'hidden' : 'visible' }}
            >{_l`Save copy`}</Button>
          </GridColumn>
          <GridColumn width={2} className={style.dflex}>
            <Dropdown
              button
              pointing={false}
              text={_l`Share`}
              className={style.btn}
              style={{ width: '50%', visibility: props.isPDF ? 'hidden' : 'visible', justifyContent: 'center' }}
            >
              <Dropdown.Menu>
                <Dropdown.Item
                  onClick={() => {
                    // if (MODE !== 'PREVIEW') {
                    //   putError(`Change to preview mode to download`);
                    //   return;
                    // } else
                    handleExport();
                  }}
                >{_l`Download PDF`}</Dropdown.Item>
                <Dropdown.Item
                  onClick={() => {
                    if (!accountAllowedSendEmail) {
                      putError(`You need to connect an Office365 or Gmail account to Salesbox first`);
                      return;
                    }
                    // if (MODE !== 'PREVIEW') {
                    //   putError(`Change to preview mode to download`);
                    //   return;
                    // }
                    getPdfToSend();
                    // showHideMassPersonalMail(true)
                  }}
                >{_l`Send via email`}</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Button
              className={style.btn}
              style={{ width: '50%' }}
              onClick={() => getCurrentQuantitySignatures()}
            >{_l`eSign`}</Button>
          </GridColumn>
        </GridRow>
      </Grid>
      <ModalCommon
        isLoadingDoneButton={isSavingTemplate}
        size="tiny"
        title={_l`Save as template`}
        visible={showModalSaveDocument}
        onDone={() => {
          setIsSavingTemplate(true);
          setTimeout(
            onDoneSaveTemplate(),
            attachmentsBlock && attachmentsBlock?.attachments.length === attachmentsBlock?.attachmentsUrl.length
              ? 1000
              : 5000
          );
        }}
        onClose={() => {
          setshowModalSaveDocument(false);
          setErrorName(null);
          setNameTemplate(null);
        }}
      >
        <div className={style.dflex} style={{ marginLeft: 5 }}>
          <div className={style.formLabel}>
            {_l`Name`}
            <span style={{ color: 'red' }}>*</span>
          </div>
          <div className={style.formField}>
            <Input
              fluid
              value={nameTemplate}
              error={errorName !== null}
              onChange={(e) => {
                setErrorName(null);
                setNameTemplate(e.target.value);
              }}
            />
            {errorName && (
              <p style={{ color: '#ed684e' }} className={style.messageError}>
                {errorName}
              </p>
            )}
          </div>
        </div>
      </ModalCommon>
      <ModalCommon
        size="tiny"
        title={_l`eSign`}
        visible={showModelESign}
        onClose={() => {
          setShowModelESign(false);
          getCompaniesDTO();
          setIsWarning([]);
        }}
        onDone={() => {
          if (isWarning.length !== 0) {
            putError('Missing phone to this method');
            return;
          }
          sendForSigning();
        }}
        isLoadingDoneButton={isSendingForSigner}
        description={false}
        scrolling={false}
        okLabel={_l`OK`}
      >
        <div className={(style.dflex, style.flexcolumn)}>
          <div style={{ marginBottom: 15 }}>
            {moment(new Date()).isAfter('2021-07-31')
              ? `You have ${currentSignatures} signatures to use`
              : _l`All Salesbox users get FREE e-Signatures until July 31st 2021`}
            {'! '}
          </div>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>{_l`Party`}</Table.HeaderCell>
                <Table.HeaderCell>{_l`Method`}</Table.HeaderCell>
                <Table.HeaderCell>{_l`Language`}</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {listAllCompanies.map((i, index) => {
                return (
                  <Table.Row style={{ padding: 0, margin: 0 }}>
                    <Table.Cell>
                      {(i.phone === '' || i.phone === null) && isWarning.includes(index) && isWarning.length && (
                        <p style={{ color: 'red', fontSize: 11 }}>{_l`Missing phone number`}</p>
                      )}
                      {i.givenName} {i.familyName}
                    </Table.Cell>
                    <Table.Cell style={{ padding: 0, margin: 0 }}>
                      <Dropdown
                        placeholder="Choose a method"
                        selection
                        search
                        fluid
                        style={{ border: 'none' }}
                        options={eIdMethods}
                        defaultValue={'email'}
                        required
                        onChange={(e, { value }) => {
                          if (value === 'sms') {
                            if (!isWarning.includes(index)) setIsWarning([...isWarning, index]);
                          }
                          handleMethodsSendForSigning(i, index, value);
                        }}
                      />
                    </Table.Cell>
                    <Table.Cell style={{ padding: 0, margin: 0 }}>
                      <Dropdown
                        placeholder="Choose language"
                        fluid
                        selection
                        search
                        style={{ border: 'none' }}
                        // value={getFromListOptions(i, 'language')}
                        defaultValue={i.language}
                        options={languageOption}
                        onChange={(e, { value }) => handleLanguageSendForSigning(i, index, value)}
                      />
                    </Table.Cell>
                  </Table.Row>
                );
              })}
            </Table.Body>
          </Table>
        </div>
      </ModalCommon>
      <ModalCommon
        size="tiny"
        title={_l`Delete version`}
        visible={showDeleteVersionModal}
        onDone={() => {
          deleteVersion(versionToDelete);
        }}
        onClose={() => {
          setShowModelDeleteVersion(false);
        }}
      >
        <div className={style.dflex} style={{ marginLeft: 5 }}>
          {_l`Do you really want to delete this copy`}
        </div>
      </ModalCommon>
      <ModalCommon
        size="tiny"
        title={_l`Delete template`}
        visible={showModalDeleteDocument}
        onDone={() => {
          deleteDocument(templateUuidToDelete);
        }}
        onClose={() => {
          setShowModelDeleteDocument(false);
        }}
      >
        <div className={style.dflex} style={{ marginLeft: 5 }}>
          {_l`Do you really want to delete this template`}
        </div>
      </ModalCommon>
      <ModalCommon
        isLoadingDoneButton={isSavingVersion}
        size="tiny"
        title={_l`Save copy`}
        visible={showModalSaveVersion}
        onDone={() => {
          setIsSavingVersion(true);
          setTimeout(
            handleCopyVersion(),
            attachmentsBlock && attachmentsBlock?.attachments.length === attachmentsBlock?.attachmentsUrl.length
              ? 1000
              : 5000
          );
        }}
        onClose={() => {
          setshowModalSaveVersion(false);
          setErrorVersionName(null);
          setVersionName(null);
        }}
      >
        <div className={style.dflex} style={{ marginLeft: 5 }}>
          <div className={style.formLabel}>
            {_l`Name`}
            <span style={{ color: 'red' }}>*</span>
          </div>
          <div className={style.formField}>
            <Input
              fluid
              value={versionName}
              error={errorVersionName !== null}
              onChange={(e) => {
                setErrorVersionName(null);
                setVersionName(e.target.value);
              }}
            />
            {errorName && (
              <p style={{ color: '#ed684e' }} className={style.messageError}>
                {errorName}
              </p>
            )}
          </div>
        </div>
      </ModalCommon>
      <ModalSendEmailAddDeal
        isModalSendCV={false}
        isSignature={true}
        visible={isShowMassPersonalEmail}
        setVisible={showHideMassPersonalMail}
        fileAttach={fileAttachSendMail}
      />
      {/* <MassPersonalEmail sendEmailInBatch={sendEmailInBatch} showHideMassPersonalMail={showHideMassPersonalMail} isShowMassPersonalEmail={isShowMassPersonalEmail} overviewType={overviewType} overviewType={overviewType} fileAttach={fileAttachSendMail} isSignature={true}/> */}
    </>
  );
};

const mapStateToProps = (state) => ({
  MODE: state?.entities?.signature?.__MODE,
  isPDF: state?.entities?.signature?.isShowPDF,
  objectType: state?.entities?.signature?.objectType,
  enterpriseId: state?.auth?.enterpriseID,
  signatureStore: state?.entities?.signature,
  listCompanies: state?.entities?.organisationDropdown,
  chosenParties: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER)?.[
    `__COMPANY_CONTACT_INFO`
  ],
  fromParties: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER)?.[
    `__USER_INFO`
  ]
    ? state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER)?.[`__USER_INFO`][0]
    : null,
  userID: state?.auth?.userId,
  attachmentsBlock: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.ATTACHMENT),
  accountAllowedSendEmail: state.common.accountAllowedSendEmail,
  __DEAL: state?.entities?.signature?.__DEAL,
  __COMPANY: state?.entities?.signature?.__COMPANY,
  __CONTACT: state?.entities?.signature?.__CONTACT,
  __USER_LOGIN_INFO: state?.entities?.signature?.__USER_LOGIN_INFO,
});

const mapDispatchToProps = {
  selectMode,
  notiSuccess: NotificationActions.success,
  putError: NotificationActions.error,
  initDataTemplateSelected,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));
