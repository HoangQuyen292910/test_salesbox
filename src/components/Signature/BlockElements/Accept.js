import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Icon } from 'semantic-ui-react';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
export const Accept = (props) => {
  const [isHovering, setIsHovering] = useState(false);

  return (
    <div
      className={style.containerElement}
      style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      <Button color="red" size="huge" content={_l`Decline`} icon={<Icon name="close" size="small" />} />
      <Button color="green" size="huge" content={_l`Accept`} icon={<Icon name="check" size="small" />} />
      {isHovering && props.__MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.ACCEPTs} index={props.index} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  __MODE: state?.entities?.signature?.__MODE,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Accept);
