import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import Dropzone from 'react-dropzone';
import { Resizable } from 're-resizable';
import {
  addImageBlock,
  editImageBlock,
  editImageBlockInColumnBlock,
  addImageBlockInColumnBlock,
} from '../signature.actions';

export const Images = (props) => {
  const {
    index,
    __MODE,
    __ELEMENTS_DATA,
    addImageBlock,
    addImageBlockInColumnBlock,
    setDisableDraggable,
    editImageBlock,
    inColumnBlock,
    parentColumnIndex,
    parentBlockIndex,
    editImageBlockInColumnBlock,
  } = props;
  const [isHovering, setIsHovering] = useState(false);
  const [imageBase64, setImageBase64] = useState(__ELEMENTS_DATA?.img ? __ELEMENTS_DATA?.img : null);
  const [width, setWidth] = useState(__ELEMENTS_DATA?.width);
  const [height, setHeight] = useState('fit-content');
  const [widthPercent, setWidthPercent] = useState('100%');
  const onDropRejected = (error) => {
    console.log('error:', error);
  };
  const imgRef = useRef(null);

  const onDrop = async (acceptedFiles) => {
    console.log('acceptFiles', acceptedFiles);
    let realwidth = null;
    let reader = new FileReader();
    if (!acceptedFiles) return;
    let imageContainer = document.getElementsByClassName(`imagesBlock${index}`)[0];
    reader.onload = function() {
      let imgTemp = new Image();
      imgTemp.onload = function() {
        console.log('real width', imgTemp.width, 'real height', imgTemp.height);
        if (imgTemp.width <= imageContainer.clientWidth) {
          setHeight(imgTemp.height);
          setWidth(imgTemp.width);
          if (inColumnBlock) {
            addImageBlockInColumnBlock(parentBlockIndex, parentColumnIndex, index, reader.result, imgTemp.width);
            editImageBlockInColumnBlock(parentBlockIndex, parentColumnIndex, index, 'width', imgTemp.width);
            editImageBlockInColumnBlock(
              parentBlockIndex,
              parentColumnIndex,
              index,
              'imgPercent',
              `${(imgTemp.width * 100) / imageContainer.clientWidth}%`
            );
          } else {
            addImageBlock(index, reader.result, imgTemp.width);
            editImageBlock(index, 'width', imgTemp.width);
            editImageBlock(index, 'imgPercent', `${(imgTemp.width * 100) / imageContainer.clientWidth}%`);
          }
          setWidthPercent(`${(imgTemp.width * 100) / imageContainer.clientWidth}%`);
        } else {
          if (inColumnBlock) {
            addImageBlockInColumnBlock(parentBlockIndex, parentColumnIndex, index, reader.result, '100%');
            setWidth(imageContainer.clientWidth);
            setWidthPercent(`100%`);
            editImageBlockInColumnBlock(parentBlockIndex, parentColumnIndex, index, 'imgPercent', `100%`);
          } else {
            addImageBlock(index, reader.result, imageContainer.clientWidth);
            setWidth(imageContainer.clientWidth);
            setWidthPercent(`100%`);
            editImageBlock(index, 'imgPercent', `100%`);
          }
        }
      };
      imgTemp.src = reader.result;
      setImageBase64(reader.result);
    };
    reader.readAsDataURL(acceptedFiles[0]);
  };
  useEffect(() => {
    let imgSaved = __ELEMENTS_DATA?.width;
    let imageContainer = document.getElementsByClassName(`imagesBlock${index}`)[0];
    if (inColumnBlock) {
      if (imgSaved?.toString()?.includes('%') && !__ELEMENTS_DATA?.imgPercent) {
        editImageBlockInColumnBlock(
          parentBlockIndex,
          parentColumnIndex,
          index,
          'width',
          (imageContainer?.clientWidth * parseInt(__ELEMENTS_DATA?.width)) / 100
        );
        setWidth((imageContainer?.clientWidth * parseInt(__ELEMENTS_DATA?.width)) / 100);
      } else {
        setWidthPercent(`${(imgSaved * 100) / imageContainer?.clientWidth}%`);
        editImageBlockInColumnBlock(
          parentBlockIndex,
          parentColumnIndex,
          index,
          'imgPercent',
          `${(imgSaved * 100) / imageContainer?.clientWidth}%`
        );
      }
    } else {
      if (imgSaved?.toString()?.includes('%') && !__ELEMENTS_DATA?.imgPercent) {
        editImageBlock(index, 'width', (imageContainer?.clientWidth * parseInt(__ELEMENTS_DATA?.width)) / 100);
        setWidth((imageContainer?.clientWidth * parseInt(__ELEMENTS_DATA?.width)) / 100);
      } else {
        setWidthPercent(`${(imgSaved * 100) / imageContainer?.clientWidth}%`);
        editImageBlock(index, 'imgPercent', `${(imgSaved * 100) / imageContainer?.clientWidth}%`);
      }
    }
  }, [__ELEMENTS_DATA.width]);
  return (
    <div
      className={style.containerElement}
      style={{ padding: inColumnBlock ? 8 : '', minHeight: inColumnBlock ? 100 : '' }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      <div style={{ width: '100%' }} className={`imagesBlock${index}`} />
      {!__ELEMENTS_DATA?.img ? (
        <Dropzone accept=".png, .jpeg, .jpg" onDrop={onDrop} onDropRejected={onDropRejected}>
          {({ getRootProps, getInputProps }) => (
            <div className={style.photoDrag} {...getRootProps()}>
              <input {...getInputProps()} />
              <strong>{_l`Drop your image here`}</strong>
            </div>
          )}
        </Dropzone>
      ) : (
        <div style={{ minWidth: '100%' }}>
          <div
            onMouseEnter={() => !inColumnBlock && setDisableDraggable(true)}
            onMouseLeave={() => !inColumnBlock && setDisableDraggable(false)}
          >
            <Resizable
              enable={inColumnBlock}
              lockAspectRatio={true}
              maxWidth={'100%'}
              className={cx(
                __ELEMENTS_DATA?.align === 'center' && style.imgBlockCenter,
                `${index}`,
                __ELEMENTS_DATA?.align === 'left' && style.imgBlockLeft,
                __ELEMENTS_DATA?.align === 'right' && style.imgBlockRight
              )}
              onResizeStop={(e, direction, ref, d) => {
                // let string = ref.attributes.style.value
                // let widthValue = string.split('; width: ').pop().split(';')[0];
                // console.log("ref", ref, "width", widthValue)
                // setHeight(heightValue)
                // let newWidth = width + d.width
                let imageContainer = document.getElementsByClassName(`imagesBlock${index}`)[0];
                let getRealImgChange = document.getElementsByClassName(
                  cx(
                    __ELEMENTS_DATA?.align === 'center' && style.imgBlockCenter,
                    `${index}`,
                    __ELEMENTS_DATA?.align === 'left' && style.imgBlockLeft,
                    __ELEMENTS_DATA?.align === 'right' && style.imgBlockRight
                  )
                );
                let marginN = window.getComputedStyle(getRealImgChange[0]);
                let newWidth =
                  imageContainer?.clientWidth -
                  parseInt(marginN?.marginLeft) -
                  parseInt(marginN?.marginRight) -
                  parseInt(window.getComputedStyle(getRealImgChange[1])?.marginLeft) -
                  parseInt(window.getComputedStyle(getRealImgChange[1])?.marginRight);
                console.log('getReal', getRealImgChange[0]?.clientWidth, 'width', Math.abs(newWidth));
                setWidth(newWidth);
                setWidthPercent(`${(Math.abs(newWidth) * 100) / imageContainer?.clientWidth}%`);
                console.log('widthPercent', widthPercent);
                if (inColumnBlock) {
                  editImageBlockInColumnBlock(parentBlockIndex, parentColumnIndex, index, 'width', Math.abs(newWidth));
                  editImageBlockInColumnBlock(parentBlockIndex, parentColumnIndex, index, 'height', 'auto');
                } else {
                  editImageBlock(index, 'width', Math.abs(newWidth));
                  editImageBlock(index, 'height', 'auto');
                }
              }}
              size={{ width: __ELEMENTS_DATA?.imgPercent || widthPercent, height: 'auto' }}
            >
              <img
                src={__ELEMENTS_DATA?.img}
                style={{ width: '100%' }}
                className={cx(
                  __ELEMENTS_DATA?.align === 'center' && style.imgBlockCenter,
                  `${index}`,
                  __ELEMENTS_DATA?.align === 'left' && style.imgBlockLeft,
                  __ELEMENTS_DATA?.align === 'right' && style.imgBlockRight
                )}
                onClick={() => {
                  __MODE === 'BUILD' && !inColumnBlock && document.getElementById(`fileImage${index}`).click();
                  __MODE === 'BUILD' && inColumnBlock && document.getElementById(`fileImage${parentBlockIndex}${parentColumnIndex}${index}`).click();
                  // __MODE === 'BUILD' && imgRef.current.click();
                }}
              />
              <input accept="image/*" type="file" hidden id={!inColumnBlock ? `fileImage${index}` : `fileImage${parentBlockIndex}${parentColumnIndex}${index}`} ref={imgRef} onChange={(e) => onDrop(e.target.files)} />
            </Resizable>
          </div>
        </div>
      )}
      {isHovering && __MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton
            type={BLOCKTYPE_SIGNATURE.IMAGE}
            index={index}
            hasEdit={true}
            inColumnBlock={inColumnBlock}
            parentColumnIndex={parentColumnIndex}
            parentBlockIndex={parentBlockIndex}
          />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state, { index, inColumnBlock, parentColumnIndex, parentBlockIndex }) => {
  let __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  if (inColumnBlock) {
    __ELEMENTS = state?.entities?.signature?.__ELEMENTS?.[parentBlockIndex]?.__ELEMENTS?.[parentColumnIndex];
  }
  return {
    __ELEMENTS_DATA: __ELEMENTS[index],
    __MODE: state?.entities?.signature?.__MODE,
  };
};

const mapDispatchToProps = {
  addImageBlock,
  editImageBlock,
  editImageBlockInColumnBlock,
  addImageBlockInColumnBlock,
};

export default connect(mapStateToProps, mapDispatchToProps)(Images);
