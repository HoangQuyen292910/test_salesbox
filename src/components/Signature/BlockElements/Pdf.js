import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Icon } from 'semantic-ui-react';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';

export const Pdf = (props) => {
  const [isHovering, setIsHovering] = useState(false);

  return (
    <div
      className={style.containerElement}
      style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      PDF
      {isHovering && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.PDF} index={props.index} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Pdf);
