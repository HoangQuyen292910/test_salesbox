import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import { editCover, setPositionBlockLastestFocused, setDataFillToEditor, setEditContent, convertOpsContent } from '../signature.actions';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.bubble.css';
import { userTags, dealTags, contactTags, companyTags, specialTags, tagMessages, ownerName, dayInPipe, dateDeal, companyCf, contactCf, dealCf } from './tagMessages';
import  moment from 'moment';

export const Cover = (props) => {
  const {
    index,
    __ELEMENTS_DATA,
    __MODE,
    setPositionBlockLastestFocused,
    valueTobeFilledToEditor,
    positionBlockLastestFocused,
    editCover, dataCFCompany, dataCFContact, dataCFDeal,
    __COMPANY,
    __CONTACT,
    __DEAL,
    __USER_LOGIN_INFO, setDataFillToEditor, setEditContent,
    positionParentBlockLastestFocused,
  } = props;
  const modules = {
    toolbar: [
      [
        'bold',
        'italic',
        'blockquote',
        {
          color: [
            '#000000',
            '#e60000',
            '#ff9900',
            '#ffff00',
            '#008a00',
            '#0066cc',
            '#9933ff',
            '#ffffff',
            '#facccc',
            '#ffebcc',
            '#ffffcc',
            '#cce8cc',
            '#cce0f5',
            '#ebd6ff',
            '#bbbbbb',
            '#f06666',
            '#ffc266',
            '#ffff66',
            '#66b966',
            '#66a3e0',
            '#c285ff',
            '#888888',
            '#a10000',
            '#b26b00',
            '#b2b200',
            '#006100',
            '#0047b2',
            '#6b24b2',
            '#444444',
            '#5c0000',
            '#663d00',
            '#666600',
            '#003700',
            '#002966',
            '#3d1466',
          ],
        },
        { background: [] },
        { header: [1, 2, 3, false] },
        ,
        { align: [] },
      ],
    ],
  };
  const [isHovering, setIsHovering] = useState(false);
  const refQuillCover = useRef(null);
  const [makeMargin, setMakeMargin ] = useState(false)
  
  const onFocusEditor = (range, source, editor) => {
    setPositionBlockLastestFocused(index);
    // updateContent(editor)
    // setMakeMargin(true)
  };

  const onBlurQuill = (_, __, editor) => {
    // updateContent(editor)
    setMakeMargin(false)
  }

  const convertToValue = ( text ) => {
    let matches = text.match(/\{{(.*?)\}}/g)
    let changeContent = text
    matches?.forEach(i => {
      if (companyTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (specialTags[checkIndex]) {
          let key = __COMPANY?.[specialTags[checkIndex]]?.name
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          let key = __COMPANY?.[tagMessages[checkIndex]]
          changeContent = changeContent.replaceAll(i, key ? key : '')
        }
      }
      if (contactTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (specialTags[checkIndex]) {
          let key = __CONTACT?.[specialTags[checkIndex]]?.name
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          let key = __CONTACT?.[tagMessages[checkIndex]]
          changeContent = changeContent.replaceAll(i, key ? key : '')
        }
      }
      if (dealTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        if (Object.keys(ownerName).some(t => checkIndex.includes(t))) {
          let key = __DEAL?.participantList?.[0][ownerName[checkIndex]]?.toString()
          changeContent = changeContent.replaceAll(i, key ? key : '')
        } else {
          // let key = __DEAL?.[checkIndex]
          if (Object.keys(dayInPipe).some(t => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(i, __DEAL[dayInPipe[checkIndex]] ? Math.ceil(__DEAL[dayInPipe[checkIndex]] / 1000 / 3600 / 24)?.toString() : '')
          }
          if (Object.keys(dateDeal).some(t => checkIndex.includes(t))) {
            changeContent = changeContent.replaceAll(i, __DEAL[dateDeal[checkIndex]] ? new moment(__DEAL[dateDeal[checkIndex]]).format('DD-MM-YYYY')?.toString() : '')
          } else {
            changeContent = changeContent.replaceAll(i, __DEAL[tagMessages[checkIndex]] ? __DEAL[tagMessages[checkIndex]] : '')
          }
        }
      }
      if (userTags.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let checkIndex = i.slice(i.indexOf("_") + 1, i.length - 2)
        let key = __USER_LOGIN_INFO?.[tagMessages[checkIndex]]
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
      if (companyCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFCompany?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value
        if (value?.customFieldValueDTOList?.[0]?.value) {
          changeContent = changeContent.replace(i, key)
        } else {
          changeContent = changeContent.replaceAll(i, '')
        }
      }
      if (contactCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFContact?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value?.toString()
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
      if (dealCf.some(t => i.slice(0, i.indexOf("_") + 1).includes(t))) {
        let thisUuid = i.slice(i.indexOf("_") + 1, i.length - 2)
        let value = dataCFDeal?.find((element) => element.title === thisUuid);
        let key = value.customFieldValueDTOList[0]?.value
        changeContent = changeContent.replaceAll(i, key ? key : '')
      }
    })
    return changeContent
  }

  // useEffect(() => {
  //   if (__MODE === "PREVIEW") {
  //     let changeContent = convertToValue(__ELEMENTS_DATA?.content)
  //     const quill = refQuillCover.current.getEditor();
  //     quill.focus();
  //     setEditContent(changeContent, index, quill.getContents())
  //   }
  // }, [__MODE, __ELEMENTS_DATA?.contentTemp,  __ELEMENTS_DATA?.content])

  useEffect(() => {
    let arrOps = __ELEMENTS_DATA?.valuePdf?.ops
    arrOps?.forEach(i => {
      i.insert = convertToValue(i.insert)
    })
    convertOpsContent(index, 'valuePdf', {ops: arrOps})
  }, [__ELEMENTS_DATA?.valuePdf])

  const updateContent = (editor) => {
    let _editor = editor;
    if (!_editor) {
      const quill = refQuillCover.current.getEditor();
      _editor = quill.getContents();
    }
    editCover('content', _editor.getHTML(), _editor.getContents(), index);
  };

  useEffect(() => {
    if (valueTobeFilledToEditor && index === positionBlockLastestFocused && !positionParentBlockLastestFocused) {
      const quill = refQuillCover.current.getEditor();
      quill.focus();
      let range = quill.getSelection();
      let position = range ? range.index : 0;
      quill.insertText(position, valueTobeFilledToEditor);
    }
  }, [valueTobeFilledToEditor]);

  const onChangeCOntent = (content, delta, source, editor) => {
    editCover('content', content, editor.getContents(), index);
    // setEditContent(content, index)
    setDataFillToEditor('');
  };
  return (
    <div
      className={style.containerElement}
      style={{
        display: 'block',
        // justifyContent: 'center',
        // alignItems: 'center',
        // position: 'relative',
        // backgroundImage: __ELEMENTS_DATA?.backgroundType === 'image' && `url(${__ELEMENTS_DATA?.backgroundImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        background:
          __ELEMENTS_DATA?.backgroundType === 'image'
            ? `url(${__ELEMENTS_DATA?.backgroundImage})`
            : __ELEMENTS_DATA?.backgroundType === 'color'
            ? __ELEMENTS_DATA?.backgroundColor
            : __ELEMENTS_DATA?.backgroundType === 'gradient' &&
              `linear-gradient(45deg, ${__ELEMENTS_DATA?.backgroundGradientStartColor} 0%, ${__ELEMENTS_DATA?.backgroundGradientEndColor} 100%)`,
      }}
      onMouseEnter={(e) => {
        // e.stopPropagation();
        setIsHovering(true);
      }}
      onMouseLeave={(e) => {
        // e.stopPropagation();
        setIsHovering(false);
      }}
    >
      <div>
        <ReactQuill
          modules={modules}
          onFocus={onFocusEditor}
          readOnly={__MODE === 'PREVIEW'}
          ref={refQuillCover}
          id="ReactQuillBubble"
          onChange={onChangeCOntent}
          onChangeSelection={(range, source, editor) =>{
            if(range && range.length !== 0 ) setMakeMargin(true)
            else setMakeMargin(false)
          }}
          style={{ height: 'fit-content', marginBottom: makeMargin && __MODE !== 'PREVIEW' ? 100 : 0 }}
          value={__ELEMENTS_DATA?.content || ''}
          // value={__MODE === 'PREVIEW' ? __ELEMENTS_DATA?.contentTemp : __ELEMENTS_DATA?.content || ''}
          className={style.reactQuill}
          onBlur={onBlurQuill}
          theme="bubble"
          scrollingContainer="#grid-column-main-layout-signature"
        />
      </div>
      {isHovering && __MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.COVER} hasEdit={true} index={props.index} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state, { index }) => {
  const __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  return {
    __ELEMENTS_DATA: __ELEMENTS[index],
    __MODE: state?.entities?.signature?.__MODE,
    valueTobeFilledToEditor: state?.entities?.signature?.valueTobeFilledToEditor,
    positionBlockLastestFocused: state?.entities?.signature?.positionBlockLastestFocused,
    positionParentBlockLastestFocused: state?.entities?.signature?.positionParentBlockLastestFocused,
    __DEAL: state?.entities?.signature?.__DEAL,
    __COMPANY: state?.entities?.signature?.__COMPANY,
    __CONTACT: state?.entities?.signature?.__CONTACT,
    __USER_LOGIN_INFO: state?.entities?.signature?.__USER_LOGIN_INFO,
  };
};

const mapDispatchToProps = {
  editCover,
  setPositionBlockLastestFocused,
  setDataFillToEditor,
  setEditContent,
  convertOpsContent
};

export default connect(mapStateToProps, mapDispatchToProps)(Cover);
