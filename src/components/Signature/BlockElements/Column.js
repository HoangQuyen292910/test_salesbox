import React, { useState } from 'react';
import { connect } from 'react-redux';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import TextElement from './Text';
import Images from './Image';

export const Column = (props) => {
  const [isHovering, setIsHovering] = useState(false);
  const BlockType = {
    TEXT: TextElement,
    IMAGE: Images,
  };
  const { __ELEMENTS_DATA, index, dataCFCompany, dataCFContact, dataCFDeal, disableDropAllColumnBlock, __MODE } = props;

  return (
    <div
      className={style.containerElement}
      style={{
        display: 'flex',
        justifyContent: __ELEMENTS_DATA?.__ELEMENTS?.length > 1 ? 'space-between' : 'center',
      }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      {__ELEMENTS_DATA?.__ELEMENTS?.map((column, indexColumn) => {
        return (
          <Droppable
            // type={`${index}`}
            droppableId={`columnBlock-${index}-${indexColumn}`}
            key={indexColumn}
            isDropDisabled={disableDropAllColumnBlock || __MODE === 'PREVIEW'}
          >
            {(provided, snapshot) => {
              const widthEachColumn =
                __ELEMENTS_DATA?.__ELEMENTS?.length > 0 ? 100 / __ELEMENTS_DATA?.__ELEMENTS?.length : '100';
              return (
                <div
                  style={{ width: `${widthEachColumn}%`, height: 'fit-content', minHeight: 100 }}
                  {...provided.droppableProps}
                  ref={provided.innerRef}
                  className={cx(snapshot.isDraggingOver && style.backgroundIsDraggingOver)}
                >
                  {column?.length > 0 ? (
                    column?.map((elementInColumn, indexElementInColumn) => {
                      const Element = BlockType[elementInColumn.type] || BlockType['TEXT'];

                      return (
                        <Draggable
                          key={indexElementInColumn}
                          index={indexElementInColumn}
                          draggableId={`columnBlock-draggable-${elementInColumn.type}-${index}-${indexColumn}-${indexElementInColumn}`}
                          isDragDisabled={__MODE === 'PREVIEW'}
                        >
                          {(subProvided, subSnapshot) => {
                            return (
                              <div
                                ref={subProvided.innerRef}
                                {...subProvided.draggableProps}
                                {...subProvided.dragHandleProps}
                                key={indexElementInColumn}
                                className={cx(
                                  subSnapshot.isDragging && style.elementIsDragging,
                                  props.__MODE === 'BUILD' && style.borderElementColumnBlock
                                )}
                              >
                                <Element
                                  inColumnBlock={true}
                                  dataCFCompany={dataCFCompany}
                                  dataCFContact={dataCFContact}
                                  dataCFDeal={dataCFDeal}
                                  index={indexElementInColumn}
                                  parentColumnIndex={indexColumn}
                                  parentBlockIndex={index}
                                  // setDisableDraggable={setDisableDraggable}
                                />
                              </div>
                            );
                          }}
                        </Draggable>
                      );
                    })
                  ) : (
                    // <div
                    //   className={cx(props.__MODE === 'BUILD' && style.borderElementColumnBlock)}
                    //   style={{ display: 'flex', justifyContent: 'center', height: '100px', alignItems: 'center' }}
                    // >
                    <p style={{ marginTop: 50, textAlign: 'center' }}>Add a block here</p>
                    // </div>
                  )}
                  {provided.placeholder}
                </div>
              );
            }}
          </Droppable>
        );
      })}
      {isHovering && props.__MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.ACCEPTs} index={props.index} hasEdit={true} />
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state, { index }) => {
  const __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  return {
    __ELEMENTS_DATA: __ELEMENTS[index],
    __MODE: state?.entities?.signature?.__MODE,
  };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Column);
