import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
// import TermOfUseModal from '../../SignUpForm/TermOfUseModal';
import ModalCommon from '../../ModalCommon/ModalCommon';
import { Input } from 'semantic-ui-react';
import { changeUrlTerm } from '../signature.actions';

export const Term = (props) => {
  const [isHovering, setIsHovering] = useState(false);
  const [isOpened, setIsOpened] = useState(false);
  const handleOpenModal = (type) => {
    setIsOpened(type);
  };
  const { __ELEMENT_DATA } = props;

  useEffect(() => {
    if (props.__ELEMENT_DATA) {
      setUrlTerm(props.__ELEMENT_DATA.urlTerm);
    }
  }, [props.__ELEMENT_DATA]);

  const [visibleAddTerm, setVisibleAddTerm] = useState(false);

  const [urlTerm, setUrlTerm] = useState('');
  const [errorTerm, setErrorTerm] = useState(false);

  const onDone = () => {
    if (!urlTerm || '' === urlTerm?.trim()) {
      setErrorTerm(true);
      return;
    }
    props.changeUrlTerm(urlTerm.trim());
    onClose();
  };
  const onClose = () => {
    setErrorTerm(false);
    setVisibleAddTerm(false);
  };

  return (
    <div
      className={style.containerElement}
      style={{ display: 'block', minHeight: 100 }}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      <div style={{ display: 'flex', justifyContent: 'center', justifyItems: 'center' }}>
        <h5>
          {_l`By approving this document you agree with the`}{' '}
          <a
            target={props.__MODE === 'BUILD' ? '_self' : '_blank'}
            href={props.__MODE === 'BUILD' ? '#' : __ELEMENT_DATA?.urlTerm ? __ELEMENT_DATA?.urlTerm : '#'}
            onClick={() => {
              props.__MODE === 'BUILD' && setVisibleAddTerm(true);
            }}
          >{_l`terms and conditions.`}</a>
        </h5>
      </div>

      {isHovering && props.__MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.TERM} index={props.index} />
        </div>
      )}
      {/* <TermOfUseModal isOpened={isOpened} termFromBlock={true} handleOpenModal={handleOpenModal}/> */}
      <ModalCommon title={_l`Terms`} visible={visibleAddTerm} onDone={onDone} onClose={onClose} size="tiny">
        <div className={style.dflex}>
          <div className={style.formLabel}>
            {_l`URL`}
            <span style={{ color: 'red' }}>*</span>
          </div>
          <div className={style.formField}>
            <Input
              type="url"
              fluid
              value={urlTerm}
              onChange={(e) => {
                setUrlTerm(e.target.value);
                setErrorTerm(false);
              }}
            />
            {errorTerm && <p style={{ color: '#ed684e' }}>{_l`URL is required`}</p>}
          </div>
        </div>
      </ModalCommon>
    </div>
  );
};

const mapStateToProps = (state) => ({
  __MODE: state?.entities?.signature?.__MODE,
  __ELEMENT_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e.type === BLOCKTYPE_SIGNATURE.TERM),
});

const mapDispatchToProps = {
  changeUrlTerm,
};

export default connect(mapStateToProps, mapDispatchToProps)(Term);
