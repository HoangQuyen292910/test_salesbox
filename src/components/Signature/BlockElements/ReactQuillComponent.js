import React, { Component } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
export class ReactQuillComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {};
    this.reactQuillRef = null;
  }

  render() {
    return (
      <ReactQuill
        theme="snow"
        ref={(el) => {
          this.reactQuillRef = el;
        }}
      />
    );
  }
}

export default ReactQuillComponent;
