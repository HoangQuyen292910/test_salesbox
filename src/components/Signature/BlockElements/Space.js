import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Icon } from 'semantic-ui-react';
import style from '../Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import GroupControllerButton from './GroupControllerButton';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
export const Space = (props) => {
  const [isHovering, setIsHovering] = useState(false);

  const {__ELEMENTS_DATA} = props;

  return (
    <div
      className={style.containerElement}
      style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', minHeight: __ELEMENTS_DATA?.height || 100, height: __ELEMENTS_DATA?.height || 100, border: '1px dashed'}}
      onMouseEnter={() => setIsHovering(true)}
      onMouseLeave={() => setIsHovering(false)}
    >
      {isHovering && props.__MODE === 'BUILD' && (
        <div className={style.GroupControllerButton}>
          <GroupControllerButton type={BLOCKTYPE_SIGNATURE.SPACE} index={props.index} hasEdit={true}/>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state, {index}) => ({
  __MODE: state?.entities?.signature?.__MODE,
  __ELEMENTS_DATA: state?.entities?.signature?.__ELEMENTS?.[index],

});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Space);
