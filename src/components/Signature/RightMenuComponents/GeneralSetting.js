import React from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import { Dropdown } from 'semantic-ui-react';
import { getLanguageOptions, getCurrencies } from 'lib/common';
import { setSignatureLanguage, setSignatureCurrency } from '../signature.actions';

export const GeneralSetting = (props) => {
  const { language, currency, setSignatureLanguage, setSignatureCurrency } = props;

  const handleChangeLanguage = (e, { value }) => {
    setSignatureLanguage(value)
  };

  const handleChangeCurrency = (e, { value }) => {
    setSignatureCurrency(value)
  }
  return (
    <div style={{ paddingTop: 65 }}>
      <h4>{_l`Language`}</h4>
      <Dropdown
        id="languageDropdown"
        type="text"
        value={language}
        placeholder={_l`Select language`}
        fluid
        search
        selection
        onChange={handleChangeLanguage}
        options={getLanguageOptions()}
      />
      <h4>{_l`Currency`}</h4>
      <Dropdown
        id="currencyDropdown"
        type="text"
        value={currency}
        placeholder={_l`Select currency`}
        fluid
        lazyLoad
        search
        selection
        onChange={handleChangeCurrency}
        options={getCurrencies()}
      />
    </div>
  );
};

const mapStateToProps = (state) => ({
  language: state?.entities?.signature?.language || state?.ui?.app?.locale || 'en',
  currency: state?.entities?.signature?.currency || state?.ui?.app?.currency,
});

const mapDispatchToProps = {
  setSignatureLanguage,
  setSignatureCurrency
};

export default connect(mapStateToProps, mapDispatchToProps)(GeneralSetting);
