import React, { useState } from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import style from '../Signature.css';
import Layout1 from '../../../../public/ESignIcon/Layout1.svg';
import Layout2 from '../../../../public/ESignIcon/Layout2.svg';
import Layout5 from '../../../../public/ESignIcon/Layout5.svg';
import cx from 'classnames';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import { editImageBlock } from '../signature.actions';
import { Divider, Button } from 'semantic-ui-react';

export const ImageSetting = (props) => {
  const align = [
    {
      key: 'left',
      icon: Layout1,
    },
    {
      key: 'center',
      icon: Layout5,
    },
    {
      key: 'right',
      icon: Layout2,
    },
  ];
  const { __ELEMENTS_DATA, editImageBlock, blockIndex } = props;

  return (
    <div style={{ paddingTop: 20 }}>
      <h4>{_l`Align`}</h4>
      <div className={style.listCoverSetting}>
        {align.map((align) => {
          return (
            <div
              onClick={() => editImageBlock(blockIndex, 'align', align.key)}
              key={align.key}
              className={cx(style.itemMenuLayout, __ELEMENTS_DATA?.align === align.key && style.itemMenuLayoutActive)}
            >
              <img src={align.icon} alt="" />
            </div>
          );
        })}
      </div>
      <Divider />
    </div>
  )
}

const mapStateToProps = (state) => {
  const blockIndex = state.entities?.signature?.blockIndexShowRightmenu;
  const __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  return {
    blockIndex,
    __ELEMENTS_DATA: __ELEMENTS[blockIndex]
  };
};

const mapDispatchToProps = {
  editImageBlock
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageSetting);