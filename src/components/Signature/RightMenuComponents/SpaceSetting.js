import React, { useState } from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import style from '../Signature.css';
import cx from 'classnames';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import { editHeightSpaceBlock } from '../signature.actions';
import { Divider, Button, Input } from 'semantic-ui-react';

export const SpaceSetting = (props) => {
  const { __ELEMENTS_DATA, editHeightSpaceBlock, blockIndex } = props;

  return (
    <div style={{ paddingTop: 20 }}>
      <h4 style={{ textTransform: 'capitalize' }}>{_l`height`}</h4>
      <div>
        <Input
          fluid
          value={__ELEMENTS_DATA.height || ''}
          onChange={(e) => {
            editHeightSpaceBlock(blockIndex, e.target.value);
          }}
          type="number"
          label={{ basic: true, content: 'px' }}
          labelPosition="right"
        />
      </div>
      <Divider />
    </div>
  );
};

const mapStateToProps = (state) => {
  const blockIndex = state.entities?.signature?.blockIndexShowRightmenu;
  const __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  return {
    blockIndex,
    __ELEMENTS_DATA: __ELEMENTS[blockIndex],
  };
};

const mapDispatchToProps = {
  editHeightSpaceBlock,
};

export default connect(mapStateToProps, mapDispatchToProps)(SpaceSetting);
