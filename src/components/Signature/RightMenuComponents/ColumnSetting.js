import React, { useState } from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import style from '../Signature.css';
import OneColumn from '../../../../public/ESignIcon/1column.svg';
import TwoColumn from '../../../../public/ESignIcon/2column.svg';
import ThreeColumn from '../../../../public/ESignIcon/3column.svg';
import cx from 'classnames';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import { Divider, Button } from 'semantic-ui-react';
import { selectTotalColumn } from '../signature.actions';

export const ColumnSetting = (props) => {
  const columns = [
    {
      key: 1,
      icon: OneColumn,
    },
    {
      key: 2,
      icon: TwoColumn,
    },
    {
      key: 3,
      icon: ThreeColumn,
    },
  ];
  const { __ELEMENTS_DATA, blockIndex, selectTotalColumn } = props;

  return (
    <div style={{ paddingTop: 20 }}>
      <h4>{_l`Columns`}</h4>
      <div className={style.listCoverSetting}>
        {columns.map((column) => {
          return (
            <div
              onClick={() => selectTotalColumn(blockIndex, column.key)}
              key={column.key}
              className={cx(
                style.itemMenuLayout,
                __ELEMENTS_DATA?.__ELEMENTS?.length === column.key && style.itemMenuLayoutActive
              )}
            >
              <img src={column.icon} alt={`iconColumn${column.key}`} width={24} />
            </div>
          );
        })}
      </div>
      <Divider />
    </div>
  );
};

const mapStateToProps = (state) => {
  const blockIndex = state.entities?.signature?.blockIndexShowRightmenu;
  const __ELEMENTS = state?.entities?.signature?.__ELEMENTS;
  return {
    blockIndex,
    __ELEMENTS_DATA: __ELEMENTS?.[blockIndex],
  };
};

const mapDispatchToProps = {
  selectTotalColumn,
};

export default connect(mapStateToProps, mapDispatchToProps)(ColumnSetting);
