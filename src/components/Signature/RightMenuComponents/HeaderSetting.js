import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import { Divider, Input, Button, Icon } from 'semantic-ui-react';
import style from '../Signature.css';
import Layout1 from '../../../../public/ESignIcon/Layout1.svg';
import Layout2 from '../../../../public/ESignIcon/Layout2.svg';
import Layout3 from '../../../../public/ESignIcon/Layout3.svg';
import Layout4 from '../../../../public/ESignIcon/Layout4.svg';
import Layout5 from '../../../../public/ESignIcon/Layout5.svg';
import Layout6 from '../../../../public/ESignIcon/Layout6.svg';
import cx from 'classnames';
import { BLOCKTYPE_SIGNATURE } from '../../../Constants';
import { editHeader, changeValueShowCropPhotoModal } from '../signature.actions';
import eSignBgDefault from '../../../../public/eSignBgDefaut.jpg';
import ModalCropPhoto from '../ModalCropPhoto';

export const Header = (props) => {
  const layouts = [
    {
      key: 'layout1',
      icon: Layout1,
    },
    {
      key: 'layout2',
      icon: Layout2,
    },
    {
      key: 'layout3',
      icon: Layout3,
    },
    {
      key: 'layout4',
      icon: Layout4,
    },
    {
      key: 'layout5',
      icon: Layout5,
    },
    {
      key: 'layout6',
      icon: Layout6,
    },
  ];

  const { __ELEMENT_DATA, editHeader, changeValueShowCropPhotoModal } = props;

  const styleInputColor = { width: 30, height: 30, padding: 0 };

  const [imgBg, setImgBg] = useState();
  const [ uploadImgWidth, setUploadImgWidth ] = useState(null)
  const [ uploadImgHeight, setUploadImgHeight ] = useState(null)

  useEffect(() => {
    if(selectedImage && uploadImgHeight && uploadImgWidth){
      setVisibleCropPhotoModal(true);
      changeValueShowCropPhotoModal(true);
    }
  }, [uploadImgHeight, uploadImgWidth])
  const selectBackground = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      // reader.addEventListener('load', () => setImgBg(reader.result));
      reader.addEventListener('load', () => {
        let imgTemp = new Image()
        imgTemp.onload = function(){
          if(imgTemp.width < __ELEMENT_DATA?.imageMainCoverSize?.width) {
            setUploadImgWidth(imgTemp.width)
          } else {
            setUploadImgWidth(__ELEMENT_DATA?.imageMainCoverSize?.width)
          } 
          if(imgTemp.height < __ELEMENT_DATA?.imageMainCoverSize?.height) {
            setUploadImgHeight(imgTemp.height)
          } else setUploadImgHeight(__ELEMENT_DATA?.imageMainCoverSize?.height)
        }
        imgTemp.src=reader.result
        setSelectedImage(reader.result);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const [visibleCropPhotoModal, setVisibleCropPhotoModal] = useState(false);
  const [selectedImage, setSelectedImage] = useState(null);
  return (
    <div style={{ paddingTop: 20 }}>
      <h4>{_l`Layouts`}</h4>
      <div className={style.listLayoutMenu}>
        {layouts.map((layout) => {
          return (
            <div
              onClick={() => editHeader('layout', layout.key)}
              key={layout.key}
              className={cx(style.itemMenuLayout, __ELEMENT_DATA?.layout === layout.key && style.itemMenuLayoutActive)}
            >
              <img src={layout.icon} alt="" />
            </div>
          );
        })}
      </div>
      <Divider />
      <h4>{_l`Background`}</h4>
      <img
        alt=""
        src={__ELEMENT_DATA?.background || eSignBgDefault}
        height={100}
        style={{ width: '100%' }}
        onClick={() => {
          document.getElementById('fileBackgroundHeader').click();
        }}
      />
      <input accept="image/*" type="file" hidden id="fileBackgroundHeader" onChange={selectBackground} />
      <Divider />
      <div className={style.dflex}>
        <h4>{_l`Background filter`}</h4>
        <input
          type="color"
          style={styleInputColor}
          value={__ELEMENT_DATA?.backgroundFilter}
          onChange={(e) => {
            editHeader('backgroundFilter', e.target.value);
            editHeader('background', null);
          }}
        />
      </div>

      <Divider />
      <div className={style.dflex}>
        <h4>{_l`Background color`}</h4>
        <input
          type="color"
          style={styleInputColor}
          value={__ELEMENT_DATA?.backgroundColor}
          onChange={(e) => editHeader('backgroundColor', e.target.value)}
        />
      </div>

      <Divider />
      <div className={style.dflex}>
        <h4>{_l`Text color`}</h4>
        <input
          type="color"
          style={styleInputColor}
          value={__ELEMENT_DATA?.textColor}
          onChange={(e) => editHeader('textColor', e.target.value)}
        />
      </div>

      <Divider />
      <div className={style.dflex}>
        <h4>{_l`Text align`}</h4>
        <Button.Group>
          <Button
            style={{ backgroundColor: __ELEMENT_DATA?.textAlign === 'left' ? '#008df2' : '' }}
            icon
            onClick={() => editHeader('textAlign', 'left')}
          >
            <Icon name="align left" />
          </Button>
          <Button
            style={{ backgroundColor: __ELEMENT_DATA?.textAlign === 'center' ? '#008df2' : '' }}
            icon
            onClick={() => editHeader('textAlign', 'center')}
          >
            <Icon name="align center" />
          </Button>
          <Button
            style={{ backgroundColor: __ELEMENT_DATA?.textAlign === 'right' ? '#008df2' : '' }}
            icon
            onClick={() => editHeader('textAlign', 'right')}
          >
            <Icon name="align right" />
          </Button>
        </Button.Group>
      </div>

      <ModalCropPhoto
        visible={visibleCropPhotoModal}
        onClose={() => {
          setVisibleCropPhotoModal(false);
          changeValueShowCropPhotoModal(false);
          setUploadImgHeight(null)
          setUploadImgWidth(null)
        }}
        onDone={(result) => {
          editHeader('background', result);
          editHeader('backgroundFilter', null);
          setVisibleCropPhotoModal(false);
          changeValueShowCropPhotoModal(false);
          setUploadImgHeight(null)
          setUploadImgWidth(null)
        }}
        uploadImgHeight={uploadImgHeight}
        uploadImgWidth={uploadImgWidth}
        rootImg={selectedImage}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  const __ELEMENTS = state?.entities?.signature?.__ELEMENTS;

  return {
    __ELEMENT_DATA: __ELEMENTS.find((e) => e?.type === BLOCKTYPE_SIGNATURE.HEADER),
  };
};

const mapDispatchToProps = {
  editHeader,
  changeValueShowCropPhotoModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
