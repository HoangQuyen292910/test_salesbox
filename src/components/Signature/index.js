import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import style from './Signature.css';
import Header from './Header';
import LeftMenu from './LeftMenu';
import RightMenu from './RightMenu';
import MainLayout from './MainLayout';
import { Grid, GridColumn, GridRow, Divider, Sidebar, Button, Icon, Dimmer, Loader } from 'semantic-ui-react';
import { DragDropContext } from 'react-beautiful-dnd';
import { BLOCKTYPE_SIGNATURE, RIGHT_MENU_TYPE_SIGNATURE, REVENUETYPE, ObjectTypes, Endpoints } from '../../Constants';
import {
  reorderElement,
  addBlock,
  setVisibleRightMenu,
  setProductData,
  setupDataParterBlock,
  resetDefaultData,
  setupDataFromDeal,
  setupDataFromCompany,
  setupDataFromContact,
  setupDataFromUserLogin,
  setRevenueType,
  reorderElementInColumnBlock,
  addElementToColumnBlock,
} from './signature.actions';
import { pdf } from '@react-pdf/renderer';
import { saveAs } from 'file-saver';
import api from 'lib/apiClient';
import { withRouter } from 'react-router-dom';
import PdfSignature from './PdfSignature.js';
import * as NotificationActions from '../Notification/notification.actions';
import _l from 'lib/i18n';
import { trim } from 'lodash';

export const Signature = (props) => {
  const {
    visibleSidebar,
    reorderElement,
    reorderElementInColumnBlock,
    addElementToColumnBlock,
    addBlock,
    setVisibleRightMenu,
    __MODE,
    __DEAL,
    __CONTACT,
    __COMPANY,
    setProductData,
    match,
    resetDefaultData,
    setupDataParterBlock,
    __ELEMENTS,
    __PRODUCT,
    __SETTING_COLUMN_PRODUCT,
    setupDataFromDeal,
    locale,
    setupDataFromCompany,
    setupDataFromContact,
    setupDataFromUserLogin,
    auth,
    isShowingModalCropPhoto,
    setRevenueType,
    putSuccess,
    currency,
    __TOTAL_VALUE_PRODUCT,
    __REVENUETYPE,
    __PARTER_BLOCK_DATA,
    isPDF,
    companyInfoFromSetting,
    __USER_LOGIN_INFO,
    __ATTACHMENT_BLOCK,
    __COMPANY_INFO,
  } = props;
  const menuPrimaryBlock = [
    {
      key: BLOCKTYPE_SIGNATURE.HEADER,
      title: 'Header',
      icon: '',
      displayOneTimes: true,
    },
    { key: BLOCKTYPE_SIGNATURE.COVER, title: 'Cover', icon: '' },
    {
      key: BLOCKTYPE_SIGNATURE.TEXT,
      title: 'Text',
      icon: '',
      displayOneTimes: false,
    },
    {
      key: BLOCKTYPE_SIGNATURE.IMAGE,
      title: 'Image',
      icon: '',
      displayOneTimes: false,
    },
    {
      key: BLOCKTYPE_SIGNATURE.PRICING,
      title: 'Priser & Paket',
      icon: '',
      displayOneTimes: true,
    },
    {
      key: BLOCKTYPE_SIGNATURE.PARTER,
      title: 'Parter',
      icon: '',
      displayOneTimes: true,
    },
    { key: BLOCKTYPE_SIGNATURE.ATTACHMENT, title: 'Attachments', icon: '' },
    { key: BLOCKTYPE_SIGNATURE.SPACE, title: 'Space', icon: '' },
    // { key: BLOCKTYPE_SIGNATURE.TERM, title: 'Terms', icon: 'terms' },
    // {
    //   key: BLOCKTYPE_SIGNATURE.ACCEPT,
    //   title: 'Accept',
    //   icon: '',
    //   displayOneTimes: true,
    // },
    {
      key: BLOCKTYPE_SIGNATURE.COLUMN,
      title: 'Column',
      icon: '',
      displayOneTimes: false,
    },
  ];

  useEffect(() => {
    if (__MODE === 'PREVIEW' && document.getElementById('grid-column-main-layout-signaturePreview')) {
      document.getElementById('grid-column-main-layout-signaturePreview').scrollTop = 0;
    }
    if (document.getElementById('grid-column-main-layout-signature')) {
      document.getElementById('grid-column-main-layout-signature').scrollTop = 0;
    }
  }, [__MODE]);
  
  const [companyData, setCompanyData ] = useState(null)
  const getCompanyData = async () => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/company/get`,
      });
      if (res) {
        setCompanyData(res)
        setupDataFromUserLogin({
          ...__USER_LOGIN_INFO,
          companyName: res?.name,
          vatNumber: __COMPANY_INFO?.vatNumber || companyInfoFromSetting?.vat || res?.vat,
          address: `${res?.address ? `${res?.address}` : ''}${__COMPANY_INFO?.state || res?.state ? `${__COMPANY_INFO?.address || res?.address ? ',' : ''} ${__COMPANY_INFO?.state || res?.state}` : ''
            }${__COMPANY_INFO?.city || res?.city ? `${__COMPANY_INFO?.state || res?.state || __COMPANY_INFO?.address || res?.address ? ',' : ''} ${__COMPANY_INFO?.city || res?.city}` : ''}${__COMPANY_INFO?.country || res?.country ? `${__COMPANY_INFO?.city || res?.city || __COMPANY_INFO?.state || res?.state || __COMPANY_INFO?.address || res?.address ? ',' : ''} ${__COMPANY_INFO?.country || res?.country}` : ''
            }`,
          street: __COMPANY_INFO?.street || res?.street || '',
          fullName: `${auth?.user?.firstName} ${auth?.user?.lastName}`,
          email: auth?.user?.email,
          phone: auth?.user?.phone,
          firstName: auth?.user?.firstName,
          lastName: auth?.user?.lastName,
          country: auth?.user?.country || '',
        })
      }
    } catch (error) {

    }
  }

  useEffect(() => {
    if(!__COMPANY_INFO || !companyData){
      getCompanyData()
    }
    setupDataFromUserLogin({
      ...__USER_LOGIN_INFO,
      companyName: __COMPANY_INFO?.name || companyData?.name,
      vatNumber: __COMPANY_INFO?.vatNumber || companyInfoFromSetting?.vat || companyData?.vat,
      address: `${__COMPANY_INFO?.address || companyData?.address ? `${__COMPANY_INFO?.address || companyData?.address}` : ''}${
        __COMPANY_INFO?.state || companyData?.state ? `${__COMPANY_INFO?.address || companyData?.address ? ',' : ''} ${__COMPANY_INFO?.state || companyData?.state}` : ''
      }${__COMPANY_INFO?.city || companyData?.city? `${__COMPANY_INFO?.state || companyData?.state || __COMPANY_INFO?.address || companyData?.address ? ',' : ''} ${__COMPANY_INFO?.city || companyData?.city}` : ''}${
        __COMPANY_INFO?.country || companyData?.country ? `${__COMPANY_INFO?.city ||companyData?.city || __COMPANY_INFO?.state || companyData?.state || __COMPANY_INFO?.address || companyData?.address ? ',' : ''} ${__COMPANY_INFO?.country || companyData?.country}` : ''
      }`,
      fullName: `${auth?.user?.firstName} ${auth?.user?.lastName}`,
      email: auth?.user?.email,
      phone: auth?.user?.phone,
      firstName: auth?.user?.firstName,
      lastName: auth?.user?.lastName,
      country: auth?.user?.country || '',
      street: __COMPANY_INFO?.street || companyData?.street || '',
    });
    if (match?.params?.id) {
      let objectType = localStorage.getItem('objectTypeSignature');
      if (objectType === ObjectTypes.Opportunity) {
        getProductOfDeal(match.params.id);
        getDealDetail(match.params.id);
      } else if (objectType === ObjectTypes.Account) {
        getCompanyDetail(match.params.id);
      } else if (objectType === ObjectTypes.Contact) {
        getContactDetail(match.params.id);
      }
      getWorkData();

      setTimeout(() => {
        if (isPDF) {
          addBlock(BLOCKTYPE_SIGNATURE.PARTER);
        }
      }, 2000);
    }
    return () => {
      setProductData([]);
    };
  }, [match]);
  
  useEffect(() => {
    return () => {
      resetDefaultData();
    };
  }, []);

  const [revenueType, setrevenueType] = useState(REVENUETYPE.START_END);

  const getWorkData = async () => {
    try {
      const data = await api.get({
        resource: `${Endpoints.Administration}/workData/workData`,
      });
      if (data) {
        let type = data.workDataWorkDataDTOList?.find((e) => e.type === 'ORDER_ROW_TYPE');
        setRevenueType(type?.value || REVENUETYPE.START_END);
      }
    } catch (error) {}
  };
  const getDealDetail = async (dealId) => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Prospect}/getDetails/${dealId}`,
      });
      if (res) {
        if (res.sponsorList && res.sponsorList[0]) {
          getContactDetail(res.sponsorList[0].uuid);
        }
        if (res.organisationId) {
          getCompanyDetail(res.organisationId);
        }
        setupDataFromDeal(res);
      }
    } catch (error) {}
  };
  const getProductOfDeal = async (dealId) => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Prospect}/orderRow/listByProspect/${dealId}`,
      });
      if (res?.orderRowDTOList) {
        setProductData(
          res.orderRowDTOList.map((e) => {
            return {
              ...e,
              value:
                e.type === 'RECURRING' && e.periodNumber
                  ? e.numberOfUnit * e.discountedPrice * e.periodNumber
                  : e.numberOfUnit * e.discountedPrice,
              cost: (e.costUnit || 0) * (e.numberOfUnit || 0),
              profit: e.numberOfUnit * e.discountedPrice - (e.costUnit || 0) * (e.numberOfUnit || 0),
            };
          })
        );
      }
    } catch (e) {}
  };
  const getContactDetail = async (contactId) => {
    try {
      console.log('COme contact:', contactId);

      const res = await api.get({
        resource: `${Endpoints.Contact}/getDetails/${contactId}`,
        query: {
          languageCode: locale,
        },
      });
      if (res) {
        setupDataFromContact(res);
        const data = {
          __COMPANY_CONTACT_INFO: {
            // companyName: { show: true, value: '' },
            // vatNumber: { show: true, value: '' },
            // address: { show: true, value: '' },
            fullName: { show: true, value: `${res.firstName} ${res.lastName}` },
            email: { show: true, value: res.email },
            phone: { show: true, value: res.phone },
            title: { show: true, value: res.title },
          },
        };
        if (res.organisationId) {
          getCompanyDetail(res.organisationId);
        }
        // setupDataParterBlock(data);
      }
    } catch (error) {}
  };
  const getCompanyDetail = async (companyId) => {
    console.log('COme company:', companyId);
    try {
      const res = await api.get({
        resource: `${Endpoints.Organisation}/getDetails/${companyId}`,
        query: {
          languageCode: locale,
        },
      });
      if (res) {
        setupDataFromCompany(res);
        // setupDataParterBlock({
        //   __COMPANY_CONTACT_INFO: {
        //     companyName: { show: true, value: res.name },
        //     vatNumber: { show: true, value: res.vatNumber },
        //     address: { show: true, value: `${res.fullAddress}` },
        //     // fullName: { show: true, value: `${res.firstName} ${res.lastName}` },
        //     // email: { show: true, value: res.email },
        //     // phone: { show: true, value: phone },
        //     // title: { show: true, value: res.title },
        //   },
        // });
      }
    } catch (error) {
      console.log(error);
    }
  };
  const onDragStart = (initial) => {
    console.log('====BEFORT START DRAGGG:', initial);
    if (initial.draggableId.includes('columnBlock-draggable')) {
      setdisableDropMainLayout(true);
      setDisableDropAllColumnBlock(false);
      return;
    }
    if (initial.draggableId.includes('mainLayout-draggable')) {
      setDisableDropAllColumnBlock(true);
      setdisableDropMainLayout(false);
      return;
    }
    if (initial.draggableId.includes('columnBlockLeftMenu')) {
      setdisableDropMainLayout(true);
      setDisableDropAllColumnBlock(false);
      return;
    }
    setDisableDropAllColumnBlock(true);
    setdisableDropMainLayout(false);
  };
  const onDragUpdate = (initial, provided) => {
    // console.log('====DRAGGG:', initial, provided);
    // if (initial.draggableId.includes('columnBlock-draggable')) {
    //   setdisableDropMainLayout(true);
    // }
  };
  const onDragEnd = (result, provided) => {
    setdisableDropMainLayout(true);
    setDisableDropAllColumnBlock(true);

    if (!result.destination) return;
    if (
      result.source.droppableId === 'droppable-leftmenu' &&
      result.destination.droppableId === 'mainLayoutDroppable'
    ) {
      if (menuPrimaryBlock[result.source.index].key === BLOCKTYPE_SIGNATURE.TEXT) {
        addBlock(menuPrimaryBlock[result.source.index].key, result.destination.index, _l`Title`);
      } else if (menuPrimaryBlock[result.source.index].key === BLOCKTYPE_SIGNATURE.COVER) {
        addBlock(menuPrimaryBlock[result.source.index].key, result.destination.index, _l`An interesting title`);
      } else if (menuPrimaryBlock[result.source.index].key === BLOCKTYPE_SIGNATURE.HEADER) {
        addBlock(menuPrimaryBlock[result.source.index].key, result.destination.index, _l`Proposal`);
      } else addBlock(menuPrimaryBlock[result.source.index].key, result.destination.index);
      return;
    }
    // if (
    //   result.source.droppableId === 'droppable-leftmenu-presentation' &&
    //   result.destination.droppableId === 'mainLayoutDroppable'
    // ) {
    //   addBlock(menuPresentation[result.source.index].key, result.destination.index);
    //   return;
    // }
    if (
      result.source.droppableId === 'mainLayoutDroppable' &&
      result.destination.droppableId === 'mainLayoutDroppable'
    ) {
      reorderElement(result.source.index, result.destination.index);
      return;
    }
    if (result.source.droppableId.includes('columnBlock') && result.destination.droppableId.includes('columnBlock')) {
      // reorder in column block

      let blockIndex = result.draggableId.split('-')[3];
      let sourceColumnIndex = result.draggableId.split('-')[4];
      let sourceElementIndex = result.draggableId.split('-')[5];
      let destinationColumnIndex = result.destination.droppableId.split('-')[2];
      let destinationElementIndex = result.destination.index;
      reorderElementInColumnBlock(
        parseInt(blockIndex),
        parseInt(sourceColumnIndex),
        parseInt(sourceElementIndex),
        parseInt(destinationColumnIndex),
        destinationElementIndex
      );
      return;
    }
    if (result.draggableId.includes('columnBlockLeftMenu') && result.destination.droppableId.includes('columnBlock')) {
      // add Text or Image block to Column Block
      let blockIndex = result.destination.droppableId.split('-')[1];
      let destinationColumnIndex = result.destination.droppableId.split('-')[2];
      let destinationElementIndex = result.destination.index;
      let blockType = result.draggableId.split('-')[1];
      if (blockType === BLOCKTYPE_SIGNATURE.TEXT) {
        addElementToColumnBlock(
          parseInt(blockIndex),
          parseInt(destinationColumnIndex),
          destinationElementIndex,
          blockType,
          _l`Title`
        );
      } else if (blockType === BLOCKTYPE_SIGNATURE.IMAGE) {
        addElementToColumnBlock(
          parseInt(blockIndex),
          parseInt(destinationColumnIndex),
          destinationElementIndex,
          blockType
        );
      }
      return;
    }
  };

  const getFileNameToSave = () => {
    let objectType = localStorage.getItem('objectTypeSignature');
    switch (objectType) {
      case ObjectTypes.Opportunity:
        return `${__DEAL?.serialNumber}: ${__DEAL?.organisationName} - ${__COMPANY_INFO?.name || companyData?.name || ''}.pdf`;
      case ObjectTypes.Contact:
        return `${__CONTACT?.firstName || ''} ${__CONTACT?.lastName || ''} - ${__COMPANY_INFO?.name || companyData?.name || ''}.pdf`;
      case ObjectTypes.Account:
        return `${__COMPANY?.name || ''} - ${__COMPANY_INFO?.name || companyData?.name || ''}.pdf`;
      default:
        return 'DocumentForSigning.pdf';
    }
  };
  const handleExport = async () => {
    setLoading(true);
    let asPdf = pdf([]);

    // let _dom = document.getElementById('grid-column-main-layout-signaturePreview');
    // let _style = getComputedStyle(_dom);
    // let _widthContainer = parseFloat(trim(_style.width, 'px')) - 2 * parseFloat(trim(_style.paddingLeft, 'px'));
    // let _heightContainer = parseFloat(trim(_style.height, 'px')) - 2 * parseFloat(trim(_style.paddingTop, 'px'));

    asPdf.updateContainer(
      <PdfSignature
        __ELEMENTS={__ELEMENTS}
        __MODE={__MODE}
        currency={currency}
        __TOTAL_VALUE_PRODUCT={__TOTAL_VALUE_PRODUCT}
        __SETTING_COLUMN_PRODUCT={__SETTING_COLUMN_PRODUCT}
        __PRODUCT={__PRODUCT}
        __REVENUETYPE={__REVENUETYPE}
        __PARTER_BLOCK_DATA={__PARTER_BLOCK_DATA}
      />
    );
    const blob = await asPdf.toBlob();
    let fileNameToSave = 'test.pdf';
    fileNameToSave = getFileNameToSave();
    setLoading(false);
    saveAs(blob, fileNameToSave);
  };

  const [fileAttachSendMail, setFileAttachSendMail] = useState(null);
  const [isShowMassPersonalEmail, showHideMassPersonalMail] = useState(false);

  const getPdfToSend = async () => {
    setLoading(true);
    let asPdf = pdf([]);
    asPdf.updateContainer(
      <PdfSignature
        __ELEMENTS={__ELEMENTS}
        __MODE={__MODE}
        currency={currency}
        __TOTAL_VALUE_PRODUCT={__TOTAL_VALUE_PRODUCT}
        __SETTING_COLUMN_PRODUCT={__SETTING_COLUMN_PRODUCT}
        __PRODUCT={__PRODUCT}
        __REVENUETYPE={__REVENUETYPE}
        __PARTER_BLOCK_DATA={__PARTER_BLOCK_DATA}
      />
    );
    const blob = await asPdf.toBlob();
    let fileNameToSave = 'test.pdf';
    fileNameToSave = getFileNameToSave();
    let file = new File([blob], fileNameToSave);
    if(__ATTACHMENT_BLOCK?.attachments) setFileAttachSendMail([file].concat(__ATTACHMENT_BLOCK?.attachments));
    else setFileAttachSendMail([file])
    setLoading(false);
    showHideMassPersonalMail(true);
  };
  const [eSignOption, setEsignOption] = useState([]);

  const [showModelESign, setShowModelESign] = useState(false);

  const [isSendingForSigner, setIsSendingForSigner] = useState(false);

  const [pdfFile, setPdfFile] = useState(null);

  const sendForSigning = async () => {
    setIsSendingForSigner(true);
    let asPdf = pdf([]);
    asPdf.updateContainer(
      <PdfSignature
        __ELEMENTS={__ELEMENTS}
        __MODE={__MODE}
        currency={currency}
        __TOTAL_VALUE_PRODUCT={__TOTAL_VALUE_PRODUCT}
        __SETTING_COLUMN_PRODUCT={__SETTING_COLUMN_PRODUCT}
        __PRODUCT={__PRODUCT}
        __REVENUETYPE={__REVENUETYPE}
        __PARTER_BLOCK_DATA={__PARTER_BLOCK_DATA}
      />
    );
    const blob = await asPdf.toBlob();
    let formData = new FormData();
    let fileOfBlob = props.isPDF ? pdfFile[0] : new File([blob], getFileNameToSave() || '');
    console.log('pdf file', fileOfBlob);
    formData.append('document', fileOfBlob);
    // formData.append('attachments', __ELEMENTS?.find((e) => e.type === BLOCKTYPE_SIGNATURE.ATTACHMENT)?.attachments);
    __ELEMENTS
      ?.find((e) => e.type === BLOCKTYPE_SIGNATURE.ATTACHMENT)
      ?.attachments?.forEach((file) => {
        formData.append('attachments', file);
      });
    let newOption = eSignOption;
    eSignOption.forEach((i) => {
      delete i.disabled;
    });
    let objType = localStorage.getItem('objectTypeSignature');
    let getId = null;
    if (objType === ObjectTypes.Contact) {
      getId = __CONTACT.uuid;
    } else if (objType === ObjectTypes.Account) {
      getId = __COMPANY.uuid;
    } else getId = __DEAL?.uuid;
    let dataDTO = {
      objectType: objType,
      objectId: getId,
      signers: newOption,
    };

    try {
      console.log(JSON.stringify(dataDTO));
      formData.append('sendForSigningDTO', JSON.stringify(dataDTO));
      const res = await api.post({
        resource: `${Endpoints.Enterprise}/e-signature/sendForSigning`,
        data: formData,
      });
      if (res) {
        setIsSendingForSigner(false);
        setShowModelESign(false);
        setEsignOption([]);
        putSuccess(`Your document has now been sent to all parties for signing`);
      }
    } catch (error) {
      setIsSendingForSigner(false);
      console.log(error);
    }
  };
  const [dataCFCompany, setDataCFCompany] = useState([]);
  const [dataCFContact, setDataCFContact] = useState([]);
  const [dataCFDeal, setDataCFDeal] = useState([]);
  const [disableDropMainLayout, setdisableDropMainLayout] = useState(true);
  const [disableDropAllColumnBlock, setDisableDropAllColumnBlock] = useState(false);

  const [loading, setLoading] = useState(false);
  return (
    <>
      {loading && (
        <Dimmer active>
          <Loader active={loading} />
        </Dimmer>
      )}
      <div className={style.conntainer}>
        <Header
          isShowMassPersonalEmail={isShowMassPersonalEmail}
          showHideMassPersonalMail={showHideMassPersonalMail}
          fileAttachSendMail={fileAttachSendMail}
          getPdfToSend={getPdfToSend}
          handleExport={handleExport}
          eSignOption={eSignOption}
          sendForSigning={sendForSigning}
          isSendingForSigner={isSendingForSigner}
          setEsignOption={setEsignOption}
          showModelESign={showModelESign}
          setShowModelESign={setShowModelESign}
          dataCFCompany={dataCFCompany}
          dataCFContact={dataCFContact}
          dataCFDeal={dataCFDeal}
        />
        <Divider style={{ marginBottom: 0 }} />
        <DragDropContext onDragEnd={onDragEnd} onDragUpdate={onDragUpdate} onDragStart={onDragStart}>
          <Grid celled="internally" style={{ width: '100%' }}>
            {__MODE === 'BUILD' && (
              <GridColumn width={__MODE === 'BUILD' ? 3 : 0}>
                <LeftMenu
                  dataCFCompany={dataCFCompany}
                  dataCFContact={dataCFContact}
                  dataCFDeal={dataCFDeal}
                  setDataCFCompany={setDataCFCompany}
                  setDataCFContact={setDataCFContact}
                  setDataCFDeal={setDataCFDeal}
                />
              </GridColumn>
              // <GridColumn width={__MODE === 'BUILD' ? 3 : 0}>{!props.isPDF && <LeftMenu />}</GridColumn>
            )}

            <GridColumn
              width={__MODE === 'BUILD' ? 12 : 16}
              // id="grid-column-main-layout-signature"
              id={__MODE === 'BUILD' ? 'grid-column-main-layout-signature' : 'grid-column-main-layout-signaturePreview'}
              style={{ display: __MODE === 'PREVIEW' ? 'flex' : 'block', justifyContent: 'center' }}
            >
              <div style={{ width: '794px', margin: '0 auto' }}>
                <MainLayout
                  dataCFCompany={dataCFCompany}
                  dataCFContact={dataCFContact}
                  dataCFDeal={dataCFDeal}
                  __ELEMENTS={__ELEMENTS}
                  __MODE={__MODE}
                  pdfFile={pdfFile}
                  setPdfFile={setPdfFile}
                  disableDropMainLayout={disableDropMainLayout}
                  disableDropAllColumnBlock={disableDropAllColumnBlock}
                />
              </div>
            </GridColumn>
            <GridColumn>
              {/* {__MODE === 'BUILD' && (
              <Button
                icon={<Icon name="setting" />}
                onClick={() => setVisibleRightMenu(true, RIGHT_MENU_TYPE_SIGNATURE.SETTING)}
              ></Button>
            )} */}
            </GridColumn>
            <Sidebar
              animation="overlay"
              onHide={() => {
                !isShowingModalCropPhoto && setVisibleRightMenu(false, null);
              }}
              id="sidebar-signature"
              direction="right"
              visible={visibleSidebar}
            >
              <RightMenu />
            </Sidebar>
          </Grid>
        </DragDropContext>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  visibleSidebar: state?.entities?.signature?.showRightMenu,
  __MODE: state?.entities?.signature?.__MODE,
  __DEAL: state?.entities?.signature?.__DEAL,
  __CONTACT: state?.entities?.signature?.__CONTACT,
  __COMPANY: state?.entities?.signature?.__COMPANY,
  __USER_LOGIN_INFO: state?.entities?.signature?.__USER_LOGIN_INFO,
  isPDF: state?.entities?.signature?.isShowPDF,
  __ELEMENTS: state?.entities?.signature?.__ELEMENTS,
  __PRODUCT: state?.entities?.signature?.__PRODUCT,
  __SETTING_COLUMN_PRODUCT: state?.entities?.signature?.__SETTING_COLUMN_PRODUCT,
  locale: state?.ui?.app?.locale,
  auth: state.auth,
  isShowingModalCropPhoto: state?.entities?.signature?.isShowingModalCropPhoto,
  currency: state?.ui?.app?.currency,
  __TOTAL_VALUE_PRODUCT: state?.entities?.signature?.__TOTAL_VALUE_PRODUCT,
  __REVENUETYPE: state?.entities?.signature?.__REVENUETYPE,
  __PARTER_BLOCK_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER),
  companyInfoFromSetting: state?.settings?.companyInfo,
  __ATTACHMENT_BLOCK: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.ATTACHMENT),
  __COMPANY_INFO: state?.settings?.companyInfo,
});

const mapDispatchToProps = {
  reorderElement,
  addBlock,
  setVisibleRightMenu,
  setProductData,
  resetDefaultData,
  setupDataParterBlock,
  setupDataFromDeal,
  setupDataFromContact,
  setupDataFromCompany,
  setupDataFromUserLogin,
  setRevenueType,
  reorderElementInColumnBlock,
  addElementToColumnBlock,
  putSuccess: NotificationActions.success,
  putError: NotificationActions.error,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Signature));
