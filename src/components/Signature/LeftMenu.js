import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Tab } from 'semantic-ui-react';
import BlockItem from './BlockItem';
import _l from 'lib/i18n';
import { Droppable, Draggable, DragDropContext } from 'react-beautiful-dnd';
import styled from 'styled-components';
import cx from 'classnames';
import css from './Signature.css';
import { BLOCKTYPE_SIGNATURE, Endpoints, ObjectTypes } from '../../Constants';
import { addBlock, setDataFillToEditor, setupDataFromUserLogin } from './signature.actions';
import api from 'lib/apiClient';
import CollapsibleBlock from './CollapsibleBlock';
import moment from 'moment';
const Item = styled.div``;
import { roundToTwoDecimals } from 'lib/math';

export const LeftMenu = (props) => {
  const { dataCFCompany, dataCFContact, dataCFDeal, setDataCFCompany, setDataCFContact, setDataCFDeal } = props
  const menuPrimaryBlock = [
    {
      key: BLOCKTYPE_SIGNATURE.HEADER,
      title: 'Header',
      icon: require('../../../public/ESignIcon/Header.svg'),
      displayOneTimes: true,
    },
    { key: BLOCKTYPE_SIGNATURE.COVER, title: 'Cover', icon: require('../../../public/ESignIcon/Cover.svg') },
    {
      key: BLOCKTYPE_SIGNATURE.TEXT,
      title: 'Text',
      icon: require('../../../public/ESignIcon/Text.svg'),
      displayOneTimes: false,
    },
    {
      key: BLOCKTYPE_SIGNATURE.IMAGE,
      title: 'Image',
      icon: require('../../../public/ESignIcon/Image.svg'),
      displayOneTimes: false,
    },
    {
      key: BLOCKTYPE_SIGNATURE.PRICING,
      title: `Priser & Paket`,
      icon: require('../../../public/ESignIcon/PriserPaket.svg'),
      displayOneTimes: true,
    },
    {
      key: BLOCKTYPE_SIGNATURE.PARTER,
      title: `Parter`,
      icon: require('../../../public/ESignIcon/Parter.svg'),
      displayOneTimes: true,
    },
    {
      key: BLOCKTYPE_SIGNATURE.ATTACHMENT,
      title: 'Attachments',
      icon: require('../../../public/ESignIcon/Attachment.svg'),
      displayOneTimes: true,
    },
    {
      key: BLOCKTYPE_SIGNATURE.SPACE,
      title: 'Space',
      icon: require('../../../public/ESignIcon/spacer.svg'),
      displayOneTimes: false,
    },
    // {
    //   key: BLOCKTYPE_SIGNATURE.TERM,
    //   title: 'Terms',
    //   icon: require('../../../public/ESignIcon/Term.svg'),
    //   displayOneTimes: true,
    // },
    // {
    //   key: BLOCKTYPE_SIGNATURE.ACCEPT,
    //   title: 'Accept',
    //   icon: require('../../../public/ESignIcon/Accept.svg'),
    //   displayOneTimes: true,
    // },
    {
      key: BLOCKTYPE_SIGNATURE.COLUMN,
      title: 'Columns',
      icon: require('../../../public/ESignIcon/2column.svg'),
      displayOneTimes: false,
      subMenu: [
        {
          key: BLOCKTYPE_SIGNATURE.TEXT,
          title: 'Text',
          icon: require('../../../public/ESignIcon/Text.svg'),
          displayOneTimes: false,
          isSubItem: true
        },
        {
          key: BLOCKTYPE_SIGNATURE.IMAGE,
          title: 'Image',
          icon: require('../../../public/ESignIcon/Image.svg'),
          displayOneTimes: false,
          isSubItem: true
        },
      ],
    },
  ];

  const [tabIndex, setTabIndex] = useState(0);
  const [listTags, setListTags] = useState([]);

  // const [dataCFCompany, setDataCFCompany] = useState([]);
  // const [dataCFContact, setDataCFContact] = useState([]);
  // const [dataCFDeal, setDataCFDeal] = useState([]);

  useEffect(() => {
    if (props.__COMPANY?.uuid) getDataCFCompany(props.__COMPANY?.uuid);
    // return () => {
    //   setDataCFCompany([]);
    // };
  }, [props.__COMPANY]);
  useEffect(() => {
    if (props.__CONTACT?.uuid) getDataCFContact(props.__CONTACT?.uuid);
    // return () => {
    //   setDataCFContact([]);
    // };
  }, [props.__CONTACT]);

  useEffect(() => {
    if (props.__DEAL?.uuid) getDataCFDeal(props.__DEAL?.uuid);
    // return () => {
    //   setDataCFDeal([]);
    // };
  }, [props.__DEAL]);

  // useEffect(() => {
  //   return () => {
  //     setDataCFDeal([]);
  //     setDataCFCompany([]);
  //     setDataCFContact([]);
  //   };
  // }, []);

  const getDataCFCompany = async (accountId) => {
    try {
      const res = await api.get({
        resource: `enterprise-v3.0/customFieldValue/listByObject`,
        query: {
          objectType: 'ACCOUNT',
          objectId: accountId,
        },
      });
      if (res) setDataCFCompany(res.customFieldDTOList);
    } catch (error) { }
  };
  const getDataCFContact = async (contactId) => {
    try {
      const res = await api.get({
        resource: `enterprise-v3.0/customFieldValue/listByObject`,
        query: {
          objectType: 'CONTACT',
          objectId: contactId,
        },
      });
      if (res) {
        setDataCFContact(res.customFieldDTOList);
      }
    } catch (error) { }
  };
  const getDataCFDeal = async (dealId) => {
    try {
      const res = await api.get({
        resource: `enterprise-v3.0/customFieldValue/listByObject`,
        query: {
          objectType: 'OPPORTUNITY',
          objectId: dealId,
        },
      });
      if (res) {
        setDataCFDeal(res.customFieldDTOList);
      }
    } catch (error) { }
  };
  const checkExistedInLayout = (e) => {
    return props.__ELEMENTS?.find((element) => element.type === e.key) && e.displayOneTimes;
  };
  const onAddBlock = (isDragging, element) => {
    if (!isDragging && !checkExistedInLayout(element)) {
      if (element.key === BLOCKTYPE_SIGNATURE.TEXT) {
        props.addBlock(element.key, null, _l`Title`);
      } else if (element.key === BLOCKTYPE_SIGNATURE.COVER) {
        props.addBlock(element.key, null, _l`An interesting title`);
      } else if (element.key === BLOCKTYPE_SIGNATURE.HEADER) {
        props.addBlock(element.key, null, _l`Proposal`)
      } else props.addBlock(element.key);
    }
  };
  // This method is needed for rendering clones of draggables
  const getRenderItem = (items, className) => (provided, snapshot, rubric) => {
    console.log('=================', provided, snapshot, rubric)
    let item = items[rubric.source.index];
    if(rubric.draggableId.includes('columnBlockLeftMenu')) {
      item = items.find(e => e.key === BLOCKTYPE_SIGNATURE.COLUMN).subMenu[rubric.source.index];
    }
    return (
      <Item
        ref={provided.innerRef}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        isDragging={snapshot.isDragging}
        style={provided.draggableProps.style}
        key={rubric.source.index}
        isDragging={snapshot.isDragging}
      >
        <BlockItem item={item} index={rubric.source.index} isDragging={snapshot.isDragging} />
      </Item>
    );
  };

  useEffect(() => {
    getUserCompany();
    getListTag();
  }, []);

  const clickAddTag = (type, e) => {
    if (!e.uuid) {
      switch (type) {
        case 'COMPANY':
          if (e.title === 'Company Type') {
            props.setDataFillToEditor("{{" + _l`Company` + "_" + _l`Type`.toString().toLowerCase() + "}}");
            // props.setDataFillToEditor(props.__COMPANY?.[`type`]?.name?.toString());
            break;
          }
          if (e.title === 'Company Industry') {
            props.setDataFillToEditor("{{" + _l`Company` + "_" + _l`Industry`.toString().toLowerCase() + "}}");
            // props.setDataFillToEditor(props.__COMPANY?.[`industry`]?.name?.toString());
            break;
          }
          if (e.title === 'Company Size') {
            props.setDataFillToEditor("{{" + _l`Company` + "_" + _l`Size`.toString().toLowerCase() + "}}");
            // props.setDataFillToEditor(props.__COMPANY?.[`size`]?.name?.toString());
            break;
          }
          if (e.title === 'Company VAT') {
            props.setDataFillToEditor("{{" + _l`Company` + "_" + _l`VAT`?.toString()?.toLowerCase()+"}}");
            break;
          }
          if (e.title === 'Company Zip Code') {
            props.setDataFillToEditor("{{" + _l`Company` + "_" + _l`Zip Code`.toString().toLowerCase().replaceAll(" ", "_") + "}}");
            break;
          }
          props.setDataFillToEditor("{{" + _l`Company` + "_" + _l.call(this, [e.value.charAt(0).toUpperCase() + e.value.slice(1)]).toString().toLowerCase() + "}}");
          // props.setDataFillToEditor(props.__COMPANY?.[e.value]?.toString());
          break;
        case 'CONTACT':
          if (e.title === 'Contact Type') {
            // props.setDataFillToEditor(props.__CONTACT?.[`type`]?.name?.toString());
            props.setDataFillToEditor("{{" + _l`Contact` + "_" + _l`Type`.toString().toLowerCase() + "}}");
            break;
          }
          if (e.title === 'Contact Industry') {
            props.setDataFillToEditor("{{" + _l`Contact` + "_" + _l`Industry`.toString().toLowerCase() + "}}");
            // props.setDataFillToEditor(props.__CONTACT?.[`industry`]?.name?.toString());
            break;
          }
          if (e.title === 'Contact Relation') {
            props.setDataFillToEditor("{{" + _l`Contact` + "_" + _l`Relation`.toString().toLowerCase() + "}}");
            // props.setDataFillToEditor(props.__CONTACT?.[`relation`]?.name?.toString());
            break;
          }
          props.setDataFillToEditor("{{" + _l`Contact` + "_" + _l.call(this, [e.title]).toString().toLowerCase().replace(" ", "_") + "}}");
          // props.setDataFillToEditor(props.__CONTACT?.[e.value]?.toString());
          break;
        case 'DEAL':
          if (e.title === 'Owner First Name') {
            // props.setDataFillToEditor(props.__DEAL?.participantList?.[0]?.firstName?.toString());
            props.setDataFillToEditor("{{" + _l`Deal` + "_" + _l.call(this, [e.title]).toString().toLowerCase().replace(" - ", "_").replaceAll(" ", "_") + "}}");
            break;
          }
          if (e.title === 'Owner Last Name') {
            // props.setDataFillToEditor(props.__DEAL?.participantList?.[0]?.lastName?.toString());
            props.setDataFillToEditor("{{" + _l`Deal` + "_" + _l.call(this, [e.title]).toString().toLowerCase().replace("- ", "").replaceAll(" ", "_") + "}}");
            break;
          }
          if (e.type === 'DATE') {
            // props.setDataFillToEditor(new moment(props.__DEAL?.[e.value]).format('DD-MM-YYYY')?.toString());
            props.setDataFillToEditor("{{" + _l`Deal` + "_" + _l`Date`.toString().toLowerCase() + "_" + _l.call(this, [e.title]).toString().toLowerCase().replace("- ", "").replaceAll(" ", "_") + "}}");
            break;
          }
          props.setDataFillToEditor("{{" + _l`Deal` + "_" + _l.call(this, [e.title]).toString().toLowerCase().replaceAll("- ", "").replaceAll(" ", "_") + "}}");
          // props.setDataFillToEditor(props.__DEAL?.[e.value]?.toString());
          break;
        // case 'USER':
        //   props.setDataFillToEditor("${user " + `${e.uuid}`.toString() + "}");
        //   // props.setDataFillToEditor(e.value);
        //   break;
      }
    } else {
      let value = null;
      switch (type) {
        case 'COMPANY':
          props.setDataFillToEditor("{{" +_l`Company`.toString().toLowerCase() + _l`Custom`.toString()+ '_'+ `${e.title}`.toString() + "}}");
          break;
        case 'CONTACT':
          props.setDataFillToEditor("{{" +_l`Contact`.toString().toLowerCase() + _l`Custom`.toString()+ '_'+ `${e.title}`.toString() + "}}");
          break;
        case 'DEAL':
          props.setDataFillToEditor("{{" +_l`Deal`.toString().toLowerCase()+ _l`Custom`.toString()+ '_'+ `${e.title}`.toString() + "}}");
          break;
        case 'USER':
          props.setDataFillToEditor("{{" + _l`User` + "_" + `${e.title}`.toString().toLowerCase().replace(' ', '_') + "}}");
          // props.setDataFillToEditor(e.value);
          break;
        // case 'COMPANY':
        //   value = dataCFCompany?.find((element) => element.uuid === e.uuid);
        //   if (value?.customFieldValueDTOList?.[0]?.value)
        //     props.setDataFillToEditor(value.customFieldValueDTOList[0].value);
        //   break;
        // case 'CONTACT':
        //   value = dataCFContact?.find((element) => element.uuid === e.uuid);
        //   if (value?.customFieldValueDTOList?.[0]?.value)
        //     props.setDataFillToEditor(value.customFieldValueDTOList[0].value);
        //   break;
        // case 'DEAL':
        //   value = dataCFDeal?.find((element) => element.uuid === e.uuid);
        //   if (value?.customFieldValueDTOList?.[0]?.value)
        //     props.setDataFillToEditor(value.customFieldValueDTOList[0].value);
        //   break;
      }
    }
    // props.setDataFillToEditor(e.value);
  };

  const initTagFromUserLogin = () => {
    const { currentUser, company, companyInfoFromSetting } = props;

    return [
      {
        uuid: 'companyName',
        title: _l`Company`,
        type: 'TEXT',
        value: company.name,
      },
      {
        title: _l`First name`,
        type: 'TEXT',
        value: currentUser.firstName,
        uuid: 'firstName',
      },
      {
        title: _l`Last name`,
        type: 'TEXT',
        value: currentUser.lastName,
        uuid: 'lastName',
      },
      {
        title: _l`Phone`,
        type: 'TEXT',
        value: currentUser.phone,
        uuid: 'phone',
      },
      {
        title: _l`Email`,
        type: 'TEXT',
        value: currentUser.email,
        uuid: 'email',
      },
      {
        title: _l`Country`,
        type: 'TEXT',
        value: currentUser.country,
        uuid: 'country',
      },
      {
        title: _l`VAT`,
        type: 'TEXT',
        value: companyInfoFromSetting?.vat || userCompany?.vat,
        uuid: 'vat',
      },
      {
        title: _l`Street`,
        type: 'TEXT',
        value: company?.street,
        uuid: 'street',
      },
      {
        title: _l`Zip Code`,
        type: 'TEXT',
        value: companyInfoFromSetting?.postalCode || userCompany?.postalCode,
        uuid: 'zipcode',
      },
      {
        title: _l`City`,
        type: 'TEXT',
        value: companyInfoFromSetting?.city || userCompany?.city,
        uuid: 'city',
      },
    ];
  };
  const [userCompany, setUserCompany] = useState(null);
  useEffect(() => {
    if (props.userLoginInfo) {
      let obj = props.userLoginInfo
      obj = {
        ...props.userLoginInfo,
        vatNumber: userCompany?.vat,
        zipCode: userCompany?.postalCode,
        city: userCompany?.city
      }
      props.setupDataFromUserLogin(obj)
    }
  }, [userCompany]);
  const getUserCompany = async () => {
    try {
      const response = await api.get({
        resource: `${Endpoints.Enterprise}/company/get`,
      });
      if (response) setUserCompany(response)
    } catch (error) {
    }
  }
  const getListTag = async () => {
    try {
      const res = await api.get({
        resource: `${Endpoints.AdvancedSearch}/quoteTemplateFields`,
      });
      if (res) {
        setListTags({
          company: [...res.companyFields, ...res.companyCustomFields],
          contact: [...res.contactFields, ...res.contactCustomFields],
          dealOrder: [...res.dealOrderFields, ...res.dealOrderCustomFields],
          user: initTagFromUserLogin(),
        });
      }
    } catch (error) { }
  };

  return props.isPDF ? (
    <></>
  ) : (
    <div>
      <div className={css.divHeaderTab}>
        <div
          className={cx(css.divHeaderTabItem, tabIndex === 0 && css.divHeaderTabItemActive)}
          onClick={() => setTabIndex(0)}
        >
          {_l`Build`}
        </div>

        <div
          className={cx(css.divHeaderTabItem, tabIndex === 1 && css.divHeaderTabItemActive)}
          onClick={() => setTabIndex(1)}
        >
          {_l`Tags`}
        </div>
      </div>
      {tabIndex === 0 ? (
        <div className={css.listTagLeftmenu}>
          <Droppable
            key="droppable-leftmenu"
            isDropDisabled
            id="droppable-leftmenu"
            droppableId="droppable-leftmenu"
            renderClone={getRenderItem(menuPrimaryBlock, props.className)}
          >
            {(provided, snapshot) => {
              return (
                <div ref={provided.innerRef} {...provided.droppableProps}>
                  {/* {_l`Primary blocks`} */}
                  {!props.isPDF &&
                    menuPrimaryBlock.map((e, index) => {
                      const shouldRenderClone = `draggableBlock-${e.key}` === snapshot.draggingFromThisWith;

                      // If create document from Account/Contact --> hide block Pricing & Service
                      if (
                        localStorage.getItem('objectTypeSignature') !== ObjectTypes.Opportunity &&
                        e.key === BLOCKTYPE_SIGNATURE.PRICING
                      )
                        return null;
                      return (
                        <div>
                          {shouldRenderClone ? (
                            <Item>
                              <BlockItem item={e} index={index} />
                            </Item>
                          ) : (
                            <Draggable
                              isDragDisabled={checkExistedInLayout(e)}
                              draggableId={`draggableBlock-${e.key}`}
                              index={index}
                            >
                              {(provided, snapshot) => (
                                <React.Fragment>
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    key={index}
                                    onClick={() => onAddBlock(snapshot.isDragging, e)}
                                  >
                                    <BlockItem
                                      disabled={checkExistedInLayout(e)}
                                      item={e}
                                      index={index}
                                      isDragging={snapshot.isDragging}
                                    />
                                  </div>
                                </React.Fragment>
                              )}
                            </Draggable>
                          )}
                          {
                            e.subMenu?.map((elementSubMenu, indexElementSubMenu) => {
                              const shouldRenderCloneSubMenu = `columnBlockLeftMenu-${elementSubMenu.key}` === snapshot.draggingFromThisWith;

                              return shouldRenderCloneSubMenu ? (
                                <Item style={{paddingLeft: 30, position:'relative'}}>
                                  <div className={css.drawBranchLine}></div>
                                  <BlockItem item={elementSubMenu} index={indexElementSubMenu} />
                                </Item>
                              ) : (
                                <Draggable
                                  isDragDisabled={false}
                                  draggableId={`columnBlockLeftMenu-${elementSubMenu.key}`}
                                  index={indexElementSubMenu}
                                >
                                  {(provided, snapshot) => (
                                    <React.Fragment>
                                      <div
                                        ref={provided.innerRef}
                                        {...provided.draggableProps}
                                        {...provided.dragHandleProps}
                                        key={index}
                                        style={{paddingLeft: 30, position:'relative'}}
                                        // onClick={() => onAddBlock(snapshot.isDragging, e)}
                                      >
                                        <div className={css.drawBranchLine}></div>
                                        <BlockItem
                                          disabled={false}
                                          item={elementSubMenu}
                                          index={indexElementSubMenu}
                                          isDragging={snapshot.isDragging}
                                        />
                                      </div>
                                    </React.Fragment>
                                  )}
                                </Draggable>
                              )
                            })
                          }
                        </div>
                      );
                    })}
                  {provided.placeholder}
                </div>
              );
            }}
          </Droppable>
          <br />
          <br />
        </div>
      ) : (
        <div className={css.listTagLeftmenu} style={{ paddingRight: 15 }}>
          <CollapsibleBlock title={_l`Company`}>
            <Droppable
              isDropDisabled
              id="droppable-leftmenu-tags-company"
              droppableId="droppable-leftmenu-tags-company"
              renderClone={getRenderItem(listTags.company, props.className)}
            >
              {(provided, snapshot) => {
                return (
                  <div ref={provided.innerRef} {...provided.droppableProps}>
                    {listTags?.company?.map((e, index) => {
                      const shouldRenderClone = `draggableBlock-tag-company-${index}` === snapshot.draggingFromThisWith;

                      return shouldRenderClone ? (
                        <Item>
                          <BlockItem item={e} index={index} />
                        </Item>
                      ) : (
                        <Draggable draggableId={`draggableBlock-tag-company-${index}`} index={index} isDragDisabled>
                          {(provided, snapshot) => (
                            <React.Fragment>
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                key={index}
                                onClick={() => (e.title === 'Company Type' && props.__COMPANY?.['type']) || (e.title === 'Company Industry' && props.__COMPANY?.['industry']) || (e.title === 'Company Size' && props.__COMPANY?.['size']) ? clickAddTag('COMPANY', e) :
                                  props.__COMPANY?.[e.value] && e.title !== 'Company Type' && e.title !== 'Company Industry' && e.title !== 'Company Size' || dataCFCompany?.find((element) => element.uuid === e.uuid)
                                    ?.customFieldValueDTOList?.[0]?.value ? clickAddTag('COMPANY', e) : null}
                              // onClick={() => onAddBlock(snapshot.isDragging, e)}
                              >
                                {!e.uuid ? (
                                  <BlockItem
                                    item={e}
                                    index={index}
                                    disabled={
                                      (e.title === 'Company Type' && props.__COMPANY?.['type']) || (e.title === 'Company Industry' && props.__COMPANY?.['industry']) || (e.title === 'Company Size' && props.__COMPANY?.['size'])
                                        ? false
                                        : props.__COMPANY?.[e.value] && e.title !== 'Company Type' && e.title !== 'Company Industry' && e.title !== 'Company Size'
                                          ? false : true
                                    }
                                    isDragging={snapshot.isDragging}
                                  />
                                ) : (
                                  <BlockItem
                                    item={e}
                                    index={index}
                                    disabled={
                                      dataCFCompany?.find((element) => element.uuid === e.uuid)
                                        ?.customFieldValueDTOList?.[0]?.value
                                        ? false
                                        : true
                                    }
                                    isDragging={snapshot.isDragging}
                                  />
                                )}
                              </div>
                            </React.Fragment>
                          )}
                        </Draggable>
                      );
                    })}
                    {provided.placeholder}
                  </div>
                );
              }}
            </Droppable>
          </CollapsibleBlock>
          {localStorage.getItem('objectTypeSignature') !== ObjectTypes.Account && (
            <CollapsibleBlock title={_l`Contact`}>
              <Droppable
                isDropDisabled
                id="droppable-leftmenu-tags-contact"
                droppableId="droppable-leftmenu-tags-contact"
                renderClone={getRenderItem(listTags.contact, props.className)}
              >
                {(provided, snapshot) => {
                  return (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                      {listTags?.contact?.map((e, index) => {
                        const shouldRenderClone =
                          `draggableBlock-tag-contact-${index}` === snapshot.draggingFromThisWith;

                        return shouldRenderClone ? (
                          <Item>
                            <BlockItem item={e} index={index} />
                          </Item>
                        ) : (
                          <Draggable draggableId={`draggableBlock-tag-contact-${index}`} index={index} isDragDisabled>
                            {(provided, snapshot) => (
                              <React.Fragment>
                                <div
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  key={index}
                                  onClick={() => (e.title === 'Contact Type' && props.__CONTACT?.['type']) || (e.title === 'Contact Industry' && props.__CONTACT?.['industry']) || (e.title === 'Contact Relation' && props.__CONTACT?.['relation']) ? clickAddTag('CONTACT', e) :
                                  props.__CONTACT?.[e.value] && e.title !== 'Contact Type' && e.title !== 'Contact Industry' && e.title !== 'Contact Relation' || dataCFContact?.find((element) => element.uuid === e.uuid)
                                    ?.customFieldValueDTOList?.[0]?.value ? clickAddTag('CONTACT', e) : null}
                                // onClick={() => onAddBlock(snapshot.isDragging, e)}
                                >
                                  {!e.uuid ? (
                                    <BlockItem
                                      item={e}
                                      index={index}
                                      disabled={
                                        (e.title === 'Contact Type' && props.__CONTACT?.['type']) || (e.title === 'Contact Industry' && props.__CONTACT?.['industry']) || (e.title === 'Contact Relation' && props.__CONTACT?.['relation'])
                                          ? false
                                          : props.__CONTACT?.[e.value] && e.title !== 'Contact Type' && e.title !== 'Contact Industry' && e.title !== 'Contact Relation'
                                            ? false : true
                                      }
                                      isDragging={snapshot.isDragging}
                                    />
                                  ) : (
                                    <BlockItem
                                      item={e}
                                      index={index}
                                      disabled={
                                        dataCFContact?.find((element) => element.uuid === e.uuid)
                                          ?.customFieldValueDTOList?.[0]?.value
                                          ? false
                                          : true
                                      }
                                      isDragging={snapshot.isDragging}
                                    />
                                  )}
                                </div>
                              </React.Fragment>
                            )}
                          </Draggable>
                        );
                      })}
                      {provided.placeholder}
                    </div>
                  );
                }}
              </Droppable>
            </CollapsibleBlock>
          )}
          {localStorage.getItem('objectTypeSignature') !== ObjectTypes.Contact &&
            localStorage.getItem('objectTypeSignature') !== ObjectTypes.Account && (
              <CollapsibleBlock title={_l`Deal/Order`}>
                <Droppable
                  isDropDisabled
                  id="droppable-leftmenu-tags-dealOrder"
                  droppableId="droppable-leftmenu-tags-dealOrder"
                  renderClone={getRenderItem(listTags.dealOrder, props.className)}
                >
                  {(provided, snapshot) => {
                    return (
                      <div ref={provided.innerRef} {...provided.droppableProps}>
                        {listTags?.dealOrder?.map((e, index) => {
                          const shouldRenderClone =
                            `draggableBlock-tag-dealOrder-${index}` === snapshot.draggingFromThisWith;

                          return shouldRenderClone ? (
                            <Item>
                              <BlockItem item={e} index={index} />
                            </Item>
                          ) : (
                            <Draggable
                              draggableId={`draggableBlock-tag-dealOrder-${index}`}
                              index={index}
                              isDragDisabled
                            >
                              {(provided, snapshot) => (
                                <React.Fragment>
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    key={index}
                                    onClick={() => props.__DEAL?.[e.value] || e.title === 'Owner First Name' || e.title === 'Owner Last Name' || dataCFDeal?.find((element) => element.uuid === e.uuid)
                                      ?.customFieldValueDTOList?.[0]?.value ? clickAddTag('DEAL', e) : null}
                                  // onClick={() => onAddBlock(snapshot.isDragging, e)}
                                  >
                                    {!e.uuid ? (
                                      <BlockItem
                                        item={e}
                                        index={index}
                                        disabled={
                                          props.__DEAL?.[e.value]
                                            ? false
                                            : e.title === 'Owner First Name' || e.title === 'Owner Last Name'
                                              ? false
                                              : true
                                        }
                                        isDragging={snapshot.isDragging}
                                      />
                                    ) : (
                                      <BlockItem
                                        item={e}
                                        index={index}
                                        disabled={
                                          dataCFDeal?.find((element) => element.uuid === e.uuid)
                                            ?.customFieldValueDTOList?.[0]?.value
                                            ? false
                                            : true
                                        }
                                        isDragging={snapshot.isDragging}
                                      />
                                    )}
                                  </div>
                                </React.Fragment>
                              )}
                            </Draggable>
                          );
                        })}
                        {provided.placeholder}
                      </div>
                    );
                  }}
                </Droppable>
              </CollapsibleBlock>
            )}
          <CollapsibleBlock title={_l`User`}>
            <Droppable
              isDropDisabled
              id="droppable-leftmenu-tags-user"
              droppableId="droppable-leftmenu-tags-user"
              renderClone={getRenderItem(listTags.user, props.className)}
            >
              {(provided, snapshot) => {
                return (
                  <div ref={provided.innerRef} {...provided.droppableProps}>
                    {listTags?.user?.map((e, index) => {
                      const shouldRenderClone = `draggableBlock-tag-user-${index}` === snapshot.draggingFromThisWith;

                      return shouldRenderClone ? (
                        <Item>
                          <BlockItem item={e} index={index} />
                        </Item>
                      ) : (
                        <Draggable draggableId={`draggableBlock-tag-user-${index}`} index={index} isDragDisabled>
                          {(provided, snapshot) => (
                            <React.Fragment>
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                                key={index}
                                onClick={() => e.value ? clickAddTag('USER', e) : null}
                              // onClick={() => console.log('USER', e)}
                              // onClick={() => onAddBlock(snapshot.isDragging, e)}
                              >
                                <BlockItem
                                  item={e}
                                  index={index}
                                  isDragging={snapshot.isDragging}
                                  disabled={e.value ? false : true}
                                />
                              </div>
                            </React.Fragment>
                          )}
                        </Draggable>
                      );
                    })}
                    {provided.placeholder}
                  </div>
                );
              }}
            </Droppable>
          </CollapsibleBlock>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  __ELEMENTS: state?.entities?.signature?.__ELEMENTS,
  __DEAL: state?.entities?.signature?.__DEAL,
  __COMPANY: state?.entities?.signature?.__COMPANY,
  __CONTACT: state?.entities?.signature?.__CONTACT,
  currentUser: state.auth?.user,
  company: state.auth?.company,
  isPDF: state?.entities?.signature?.isShowPDF,
  companyInfoFromSetting: state?.settings?.companyInfo,
  userLoginInfo: state?.entities?.signature?.__USER_LOGIN_INFO,
});

const mapDispatchToProps = {
  addBlock,
  setDataFillToEditor,
  setupDataFromUserLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(LeftMenu);
