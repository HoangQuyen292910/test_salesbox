import React, { useState, useEffect } from 'react';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import style from './Signature.css';
import cx from 'classnames';
import Header from './BlockElements/Header';
import Parties from './BlockElements/Parties';
import TextElement from './BlockElements/Text';
import PricingService from './BlockElements/PricingService';
import Accept from './BlockElements/Accept';
import Column from './BlockElements/Column';
import Cover from './BlockElements/Cover';
import Term from './BlockElements/Term';
import Attachment from './BlockElements/Attachment';
import Images from './BlockElements/Image';
import { connect } from 'react-redux';
import _l from 'lib/i18n';
import Dropzone from 'react-dropzone';
import { pdfjs, Document, Page } from 'react-pdf';
import { Button, Icon, Dimmer, Loader } from 'semantic-ui-react';
import { addBlock } from './signature.actions';
import { BLOCKTYPE_SIGNATURE } from '../../Constants';
import { pdf } from '@react-pdf/renderer';
import PdfSignature from './PdfSignature';
import Space  from './BlockElements/Space';
pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

// import PDFViewer from 'mgr-pdf-viewer-react';
// import { Document, Page, pdfjs } from 'react-pdf/dist/esm/entry.webpack';
// pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
// pdfjs.GlobalWorkerOptions.workerSrc = 'pdf.worker.min.js';
export const MainLayout = (props) => {
  const BlockType = {
    HEADER: Header,
    PARTER: Parties,
    PRICING: PricingService,
    TEXT: TextElement,
    ACCEPT: Accept,
    COVER: Cover,
    TERM: Term,
    ATTACHMENT: Attachment,
    IMAGE: Images,
    COLUMN: Column,
    SPACE: Space
  };
  const {
    __ELEMENTS, __MODE, isPDF, pdfFile, setPdfFile,
    __COMPANY, __CONTACT,
    dataCFCompany, dataCFContact, dataCFDeal,
    currency, __TOTAL_VALUE_PRODUCT,__SETTING_COLUMN_PRODUCT, __PRODUCT, __REVENUETYPE, __PARTER_BLOCK_DATA,
    disableDropMainLayout,
    disableDropAllColumnBlock,
  } = props;

  useEffect(() => {
    pdfjs.GlobalWorkerOptions.workerSrc = `https://cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  }, []);

  const checkExistedInLayout = (e) => {
    return props.__ELEMENTS?.find((element) => element.type === e.key) && e.displayOneTimes;
  };

  // useEffect(() => {
  //   // if (
  //   //   isPDF &&
  //   //   !checkExistedInLayout({
  //   //     key: BLOCKTYPE_SIGNATURE.PARTER,
  //   //     title: _l`Parties`,
  //   //     // icon: require('../../../public/ESignIcon/Parter.svg'),
  //   //     displayOneTimes: true,
  //   //   })
  //   // ) {
  //   //   props.addBlock(BLOCKTYPE_SIGNATURE.PARTER);
  //   // }
  //   props.addBlock(BLOCKTYPE_SIGNATURE.PARTER);

  // }, [isPDF, __COMPANY, __CONTACT]);

  const [loading, setLoading] = useState(false)
  useEffect(() => {
    if(__MODE === 'PREVIEW' && !isPDF && __ELEMENTS){
      getPdfFile()
    } else if(!__ELEMENTS) setPdfBase64(null)
  }, [__MODE, __ELEMENTS])

  const getPdfFile = async () => {
    setLoading(true)
    let asPdf = pdf([]);
    asPdf.updateContainer(
      <PdfSignature
        __ELEMENTS={__ELEMENTS}
        __MODE={__MODE}
        currency={currency}
        __TOTAL_VALUE_PRODUCT={__TOTAL_VALUE_PRODUCT}
        __SETTING_COLUMN_PRODUCT={__SETTING_COLUMN_PRODUCT}
        __PRODUCT={__PRODUCT}
        __REVENUETYPE={__REVENUETYPE}
        __PARTER_BLOCK_DATA={__PARTER_BLOCK_DATA}
      />
    );
    const blob = await asPdf.toBlob();
    let fileNameToSave = 'test.pdf';
    let newFile = new File([blob], fileNameToSave);
    const result = await toBase64(newFile);
    if(result !== pdfBase64) {
      setPdfBase64(result)
      setPageNumber(1)
    }
    setLoading(false)
  }

  const onDropRejected = (error) => {
    console.log('error:', error);
  };
  const [pdfBase64, setPdfBase64] = useState(null);
  const onDrop = async (acceptedFiles) => {
    console.log(acceptedFiles);
    setPdfFile(acceptedFiles);
    if (!acceptedFiles) return;
    const result = await toBase64(acceptedFiles[0]);

    // setPdfFile(result);
    setPdfBase64(result);
  };
  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  // const [pdfFile, setPdfFile] = useState(null);

  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  const [disableDraggable, setDisableDraggable] = useState(false);

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };
  return isPDF ? (
    <div>
      {!pdfFile ? (
        <Dropzone accept=".pdf, application/pdf" onDrop={onDrop} onDropRejected={onDropRejected}>
          {({ getRootProps, getInputProps }) => (
            <div className={style.photoDrag} {...getRootProps()}>
              <input {...getInputProps()} />
              <strong>{_l`Drop your .PDF file here`}</strong>
            </div>
          )}
        </Dropzone>
      ) : (
        <div className={style.mainLayoutContainer} id="previewPDFESign" style={{ paddingBottom: 10 }}>
          <Document
            file={pdfBase64}
            onLoadSuccess={onDocumentLoadSuccess}
            style={{ display: 'flex', justifyContent: 'center' }}
          >
            <Page pageNumber={pageNumber} />
          </Document>

          <div className={style.dflex} style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Button.Group>
              <Button
                className={style.btnPagingPDF}
                onClick={() => setPageNumber(1)}
                icon={<Icon name="angle double left" />}
                disabled={pageNumber === 1}
              ></Button>
              <Button
                className={style.btnPagingPDF}
                onClick={() => setPageNumber(pageNumber - 1)}
                icon={<Icon name="angle left" />}
                disabled={pageNumber === 1}
              ></Button>
            </Button.Group>
            <p style={{ margin: '0 5' }}>{`Page ${pageNumber} of ${numPages}`}</p>
            <Button.Group>
              <Button
                className={style.btnPagingPDF}
                onClick={() => setPageNumber(pageNumber + 1)}
                icon={<Icon name="angle right" />}
                disabled={pageNumber === numPages}
              ></Button>
              <Button
                className={style.btnPagingPDF}
                onClick={() => setPageNumber(numPages)}
                icon={<Icon name="angle double right" />}
                disabled={pageNumber === numPages}
              ></Button>
            </Button.Group>
          </div>
        </div>
      )}
      {__ELEMENTS?.map((e, index) => {
        const Element = BlockType[e.type] || BlockType['HEADER'];
        return (
          <Element dataCFCompany={dataCFCompany} dataCFContact={dataCFContact} dataCFDeal={dataCFDeal} index={index} />
        );
      })}
      {/* <Parties/> */}
    </div>
  ) : (
    __MODE === "BUILD" ?
    <Droppable droppableId="mainLayoutDroppable" id="mainLayoutDroppable" isDropDisabled={disableDropMainLayout}>
      {(provided, snapshot) => {
        return (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            className={cx(
              style.mainLayoutContainer,
              __ELEMENTS?.length > 0 ? '' : style.dlfexCenter,
              snapshot.isDraggingOver && style.backgroundIsDraggingOver
            )}
          >
            {__ELEMENTS?.length > 0 ? (
              __ELEMENTS?.map((e, index) => {
                const Element = BlockType[e.type] || BlockType['HEADER'];
                return (
                  <Draggable
                    index={index}
                    draggableId={`mainLayout-draggable-${e.type}-${index}`}
                    isDragDisabled={__MODE === 'PREVIEW' || disableDraggable}
                  >
                    {(provided, snapshot) => {
                      return (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                          key={index}
                          className={cx(snapshot.isDragging && style.elementIsDragging)}
                        >
                          <Element
                            dataCFCompany={dataCFCompany}
                            dataCFContact={dataCFContact}
                            dataCFDeal={dataCFDeal}
                            index={index}
                            setDisableDraggable={setDisableDraggable}
                            disableDropAllColumnBlock={disableDropAllColumnBlock}
                          />
                        </div>
                      );
                    }}
                  </Draggable>
                );
              })
            ) : (
              <p>{_l`Start with drag & drop a block from left menu`}</p>
            )}
            {provided.placeholder}
          </div>
        );
      }}
    </Droppable>
    :
    <>
    {loading && (
        <Dimmer active>
          <Loader active={loading} />
        </Dimmer>
      )}
      <div className={style.mainLayoutContainer} id="previewPDFESign" style={{ paddingBottom: 10 }}>
          <Document
            file={pdfBase64}
            onLoadSuccess={onDocumentLoadSuccess}
            style={{ display: 'flex', justifyContent: 'center' }}
          >
            <Page scale={794/1240} pageNumber={pageNumber} />
          </Document>

          <div className={style.dflex} style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Button.Group>
              <Button
                className={style.btnPagingPDF}
                onClick={() => {
                  setPageNumber(1)
                  document.getElementById("grid-column-main-layout-signaturePreview").scrollTop = 0
                }}
                icon={<Icon name="angle double left" />}
                disabled={pageNumber === 1}
              ></Button>
              <Button
                className={style.btnPagingPDF}
                onClick={() => {
                  setPageNumber(pageNumber - 1)
                  document.getElementById("grid-column-main-layout-signaturePreview").scrollTop = 0
                }}
                icon={<Icon name="angle left" />}
                disabled={pageNumber === 1}
              ></Button>
            </Button.Group>
            <p style={{ margin: '0 5' }}>{`Page ${pageNumber} of ${numPages}`}</p>
            <Button.Group>
              <Button
                className={style.btnPagingPDF}
                onClick={() =>{
                  setPageNumber(pageNumber + 1)
                  document.getElementById("grid-column-main-layout-signaturePreview").scrollTop = 0
                }}
                icon={<Icon name="angle right" />}
                disabled={pageNumber === numPages}
              ></Button>
              <Button
                className={style.btnPagingPDF}
                onClick={() => {
                  setPageNumber(numPages)
                  document.getElementById("grid-column-main-layout-signaturePreview").scrollTop = 0
                }}
                icon={<Icon name="angle double right" />}
                disabled={pageNumber === numPages}
              ></Button>
            </Button.Group>
          </div>
        </div>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    isPDF: state?.entities?.signature?.isShowPDF,
    __ELEMENTS: state?.entities?.signature?.__ELEMENTS,
    __ELEMENT_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER),
    __COMPANY: state?.entities?.signature?.__COMPANY,
    __CONTACT: state?.entities?.signature?.__CONTACT,
    __MODE: state?.entities?.signature?.__MODE,
    currency: state?.ui?.app?.currency,
    __TOTAL_VALUE_PRODUCT: state?.entities?.signature?.__TOTAL_VALUE_PRODUCT,
    __REVENUETYPE: state?.entities?.signature?.__REVENUETYPE,
    __PARTER_BLOCK_DATA: state?.entities?.signature?.__ELEMENTS?.find((e) => e?.type === BLOCKTYPE_SIGNATURE.PARTER),
    __PRODUCT: state?.entities?.signature?.__PRODUCT,
    __SETTING_COLUMN_PRODUCT: state?.entities?.signature?.__SETTING_COLUMN_PRODUCT,
  };
};
const mapDispatchToProps = {
  addBlock,
};
export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);
