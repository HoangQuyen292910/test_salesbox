import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import _l from 'lib/i18n';
import api from 'lib/apiClient';
import { Endpoints } from '../../Constants';

export const TemplateDropdown = (props) => {
  const { objectType, templates } = props;

  return (
    <Dropdown
      noResultsMessage={_l`No results found`}
      lazyLoad
      selection
      options={templates}
      search
      placeholder={_l`Select template`}
      {...props}
    ></Dropdown>
  );
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(TemplateDropdown);
