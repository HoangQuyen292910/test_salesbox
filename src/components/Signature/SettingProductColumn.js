import React from 'react';
import { connect } from 'react-redux';
import { Menu, Popup } from 'semantic-ui-react';
import css from './Signature.css';
import _l from 'lib/i18n';
import cx from 'classnames';
import commonCss from 'Common.css';
import { CssNames, REVENUETYPE } from 'Constants';
import { toggleColumnProductBlock } from './signature.actions';

export const SettingProductColumn = (props) => {
  const popupStyle = {
    padding: 0,
  };

  const handleToggle = (key) => {
    props.toggleColumnProductBlock(key);
  };
  const colums = [
    { key: 'product', value: _l`Product` },
    { key: 'productGroup', value: _l`Product group` },
    { key: 'productType', value: _l`Product type` },
    { key: 'startDate', value: _l`Start date` },
    { key: 'endDate', value: _l`End date` },
    { key: 'unitAmount', value: _l`Unit amount` },
    { key: 'unitPrice', value: _l`Unit price` },
    { key: 'unitCost', value: _l`Unit cost` },
    { key: 'discountPercent', value: _l`Discount percentage` },
    { key: 'discountPrice', value: _l`Discounted price` },
    { key: 'discountAmount', value: _l`Discount amount` },
    { key: 'margin', value: _l`Margin %` },
    { key: 'cost', value: _l`Cost` },
    { key: 'value', value: _l`Value` },
    { key: 'occupied', value: _l`Occupied` },
    { key: 'profit', value: _l`Profit` },
    { key: 'description', value: _l`Description` },
    // { key: 'currency', value: _l`Currency` },
  ];

  const enabledIcon = (
    <div className={css.setDone}>
      <div></div>
    </div>
  );
  const disabledIcon = (
    <div className={css.notSetasDone}>
      <div></div>
    </div>
  );
  return (
    <Popup
      trigger={
        <div className={css.bgMore} icon="ellipsis vertical">
          <img style={{ height: 12 }} src={require('../../../public/3_dots.svg')} />
        </div>
      }
      flowing
      hoverable
      style={popupStyle}
      position="left center"
      className={cx(commonCss.popup, CssNames.Pipeline)}
      keepInViewPort
      hideOnScroll
    >
      <Menu vertical className={css.popMenu}>
        {colums.map((e) => {
          if (e?.key == 'occupied' && props.newIndustry != 'IT_CONSULTANCY') {
            return null;
          }
          if ((e?.key === 'startDate' || e?.key === 'endDate') && props.__REVENUETYPE === REVENUETYPE.FIXED_RECURRING)
            return null;
          return (
            <Menu.Item key={e.key} icon onClick={() => handleToggle(e.key)} className={css.columnLabel}>
              {e.value}
              {props.__SETTING_COLUMN_PRODUCT?.[e.key] ? enabledIcon : disabledIcon}
              {/* {!enabled && disabledIcon} */}
            </Menu.Item>
          );
        })}
      </Menu>
    </Popup>
  );
};

const mapStateToProps = (state) => ({
  __SETTING_COLUMN_PRODUCT: state?.entities?.signature?.__SETTING_COLUMN_PRODUCT,
  newIndustry: state.auth?.company?.newIndustry,
  __REVENUETYPE: state?.entities?.signature?.__REVENUETYPE,
});

const mapDispatchToProps = {
  toggleColumnProductBlock,
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingProductColumn);
