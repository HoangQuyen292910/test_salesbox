// @flow
import * as React from 'react';
import { defaultProps, compose, withHandlers, withState } from 'recompose';
import cx from 'classnames';
import { Segment, Menu, Header, Popup, Button, Icon } from 'semantic-ui-react';
import _l from 'lib/i18n';
import css from './MoreMenu.css';
import { DOCUMENT_STATUS_ESIGNATURE } from 'Constants';

type PropsType = {
  children: React.Node,
  open: boolean,
  onClick: () => void,
  onOpen: () => void,
  onClose: () => void,
  handleClick: () => void,
  color: string,
  title: string,
  className: string,
};

addTranslations({
  'en-US': {
    Actions: 'Actions',
  },
});

const headerStyle = {
  margin: 0,
};

const popupStyle = {
  padding: 0,
};

const MoreMenu = ({
  children,
  open,
  handleClick,
  onOpen,
  onClose,
  color,
  className,
  filter,
  sort,
  imageClass,
  position = 'bottom center',
  on,
  isDocument,
  documentStatus,
  moveIcon
}: PropsType) => {
  return (
    <Popup
      onClick={handleClick}
      trigger={
        <div className={className} onClick={handleClick} icon="ellipsis vertical">
          {!isDocument && ( sort ? (
            <Icon style={{ margin: '0px' }} name="sort" />
          ) : filter ? (
            <img className={`${css.filter} ${imageClass}`} src={require('../../../public/new_filter.svg')} />
          ) : (
            <img
              className={imageClass}
              style={{ height: '20px !important' }}
              src={require('../../../public/3_dots.svg')}
            />
          ))}
          {isDocument && !documentStatus ? <img
            style={{ height: '18px', width: '18px', marginLeft: moveIcon ? moveIcon : 0 }}
            src={require('../../../public/docsCreate.svg')} />
            :
            <>
              {documentStatus === DOCUMENT_STATUS_ESIGNATURE.SIGNED && <img
                style={{ height: '18px', width: '18px', marginLeft: moveIcon ? moveIcon : 0 }}
                src={require('../../../public/doc-status-signed.svg')} />
              }
              {documentStatus === DOCUMENT_STATUS_ESIGNATURE.SENT &&
                <img
                  style={{ height: '18px', height: '18px', marginLeft: moveIcon ? moveIcon : 0 }}
                  src={require('../../../public/doc-status-sent.svg')} />
              }
              {documentStatus === DOCUMENT_STATUS_ESIGNATURE.NO_DOCUMENT &&
                <img
                  style={{ height: '18px', width: '18px', marginLeft: moveIcon ? moveIcon : 0 }}
                  src={require('../../../public/docsCreate.svg')} />
              }
              {documentStatus === DOCUMENT_STATUS_ESIGNATURE.REJECTED &&
                <img
                  style={{ height: '18px', width: '18px', marginLeft: moveIcon ? moveIcon : 0 }}
                  src={require('../../../public/doc-status-declined.svg')} />
              }
            </>
          }
        </div>
      }
      onOpen={onOpen}
      onClose={onClose}
      open={open}
      flowing
      hoverable
      style={popupStyle}
      position={position}
      className={cx(color)}
      keepInViewPort
      hideOnScroll
      // on={on}
      on="click"
    >
      <Menu vertical color="teal">
        {/* <Segment inverted style={headerStyle}>
          <Header as="h3">{title}</Header>
        </Segment> */}
        {children}
      </Menu>
    </Popup>
  );
};

export default compose(
  defaultProps({
    title: _l`Actions`,
  }),
  withState('open', 'setOpen', false),
  withHandlers({
    onOpen: ({ setOpen }) => () => {
      setOpen(true);
    },
    handleClick: ({ setOpen }) => (event: Event) => {
      event.preventDefault();
      event.stopPropagation();
      setOpen(false);
    },
    onClose: ({ setOpen }) => () => {
      setOpen(false);
    },
  })
)(MoreMenu);
