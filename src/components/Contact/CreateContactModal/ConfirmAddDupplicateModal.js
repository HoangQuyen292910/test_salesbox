import React from 'react';
import { connect } from 'react-redux';
import ModalCommon from '../../ModalCommon/ModalCommon';
import _l from 'lib/i18n';
import { clearHighlight } from '../../Overview/overview.actions';
import { isHighlightAction, getHighlighted, getItemSelected } from '../../Overview/overview.selectors';
import { uploadErrors, requestCreate, succeedCreate } from '../contact.actions';

export const ConfirmAddDupicateModal = (props) => {
  const { visible, clearHighlight, overviewType, messageError, overviewTypeRoot } = props;

  const onDone = () => {
    props.requestCreate(overviewTypeRoot.overviewType, null, true);
    clearHighlight(overviewType);
  };

  const onClose = () => {
    clearHighlight(overviewType);
  };
  let message = '';
  switch (messageError) {
    case 'DUPLICATE_CONTACT_BY_NAME_AND_COMPANY':
      message = _l`Oh, a contact with the same information already exist (Same company name, contact first name, contact last name). Do you want to update this contact?`;
      break;
    case 'DUPLICATE_CONTACT_BY_NAME_AND_PHONE':
      message = _l`Oh, a contact with the same information already exist (Same contact first name, contact last name, phone). Do you want to update this contact?`;
      break;
    case 'DUPLICATE_CONTACT_BY_NAME_AND_CITY_AND_STREET':
      message = _l`Oh, a contact with the same information already exist (Same contact first name, contact last name, city, street). Do you want to update this contact?`;
      break;
    case 'DUPLICATE_CONTACT_BY_EMAIL':
      message = `This email already exist on ${overviewTypeRoot?.contactName}, do you want to update this contact?`;
      break;
    case 'DUPLICATE_CONTACT_BY_NAME_WITHOUT_COMPANY':
      message = _l`Oh, a contact with the same information already exist (Same contact first name, contact last name). Do you want to update this contact?`;
      break;
  }
  return (
    <ModalCommon size="tiny" title={_l`Confirm`} visible={visible} onDone={onDone} onClose={onClose}>
      <p>{message}</p>
    </ModalCommon>
  );
};

const mapStateToProps = (state, { overviewType }) => {
  const visible = isHighlightAction(state, overviewType, 'confirmAddDuplicateContact');
  const messageError = getHighlighted(state, overviewType);
  const overviewTypeRoot = getItemSelected(state, overviewType);
  return {
    visible,
    messageError: messageError,
    overviewTypeRoot
  };
};

const mapDispatchToProps = {
  clearHighlight,
  requestCreate,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmAddDupicateModal);
