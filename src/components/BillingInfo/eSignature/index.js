import React, { useEffect, useState } from 'react';
import { Grid } from 'semantic-ui-react';
import _l from 'lib/i18n';
import _ from 'lodash';
import { checkVAT, countries } from 'jsvat';
import valid from 'card-validator';
import { connect } from 'react-redux';
import * as NotificationActions from 'components/Notification/notification.actions';
import BillingPane from '../Pane';
import { Endpoints, popupWindow } from '../../../Constants';
import Licenses from '../Subscriptions/Licenses';
import api from '../../../lib/apiClient';
import CommonBilling from '../Subscriptions/common';
import BillingInfo from '../Subscriptions/BillingInfo';
import BillingModal from '../Subscriptions/BillingModal';
import Card from '../Subscriptions/Card';
import moment from 'moment';

const signPackages = {
  TWO_SIGNATURES: '2',
  TEN_SIGNATURES: '10',
  TWENTY_FIVE_SIGNATURES: '25',
  FIFTY_SIGNATURES: '50',
  HUNDERED_SIGNATURES: '100'
}

const SEKCountries = CommonBilling.SEKCountries;
const EURCountries = CommonBilling.EURCountries;

const ESignatureComponent = () => {
  const [currency, setCurrency] = useState('USD');
  const [addOnSelecteds, setAddOneSelecteds] = useState({});
  const [__extraPackage, setExtraPackage] = useState({});
  const [card, setCard] = useState({});
  const [error, setError] = useState({});
  const [invoiceDate, setInvoiceDate] = useState(new Date());
  const [isConnectWithStripe, setConnectWithStripe] = useState(false);
  const [submitting, setSubmitting] = useState(false);
  const [vatPercent, setVatPercent] = useState(0);
  const [discountPercent, setDiscountPercent] = useState(0);
  const [submittingSubscribe, setSubmittingSubscribe] = useState(false);
  const [coupon, setCoupon] = useState(null);
  const [currentQuantity, setCurrentQuantity] = useState(0)
  const [totalQuatityMessege, setTotalQuatityMessege] = useState(0);
  const [showModal, setShowModal] = useState(false);
  const [pendingCancel, setPendingCancel] = useState(false);
  const [totalAddSignature, setTotalAddSignature] = useState(0)
  
  useEffect(() => {
    getCompanyInfo();
    getPaymentInfo();
    getCardInfo();
    getCurrentQuantitySignatures();
  }, []);

  const calculateAmount = () => {
    let subTotal = 0
    let totalQuatity = 0
    for(var key in addOnSelecteds){
      subTotal += addOnSelecteds[key]['price']
      totalQuatity = parseInt(currentQuantity) + parseInt(key)
    }
    let total = subTotal + (subTotal * parseInt(vatPercent)) / 100;
    if (discountPercent) {
      total = (total * (100 - discountPercent)) / 100;
    }
    return {
      totalQuatity,
      subTotal,
      total,
    };
  };
  const { totalQuatity, subTotal, total } = calculateAmount();

  const listAddOn=[
    {
      packageName: _l`2 signatures`,
      packageId: {
        USD: 'price_1IZ6B9C7cXVm3yoBEleFS95B',
        EUR: 'price_1IZ6BAC7cXVm3yoBZsN5LEMP',
        SEK: 'price_1IZ6B9C7cXVm3yoBxeZG9eAi'
      },
      price: {
        USD: 5.9,
        EUR: 5.9,
        SEK: 59,
      },
      field: '2',
      packageType: signPackages.TWO_SIGNATURES,
      tooltip: `<p><b>${_l`2 signatures`}</b></p>`
    },
    {
      packageName: _l`10 signatures`,
      packageId: {
        USD: 'price_1IZ6BAC7cXVm3yoB9qQ5Qp9j',
        EUR: 'price_1IZ6BAC7cXVm3yoBzR7U9jp3',
        SEK: 'price_1IZ6B9C7cXVm3yoBDPATccCr' 
      },
      price: {
        USD: 27.9,
        EUR: 27.9,
        SEK: 279,
      },
      field: '10',
      packageType: signPackages.TEN_SIGNATURES,
      tooltip: `<p><b>${_l`10 signatures`}</b></p>`
    },
    {
      packageName: _l`24 signatures`,
      packageId: {
        USD: 'price_1IZ6BAC7cXVm3yoBvOdCxU69',
        EUR: 'price_1IZ6BAC7cXVm3yoBC8mCZBsT',
        SEK: 'price_1IZ6B9C7cXVm3yoBilNReOCw' 
      },
      price: {
        USD: 59.9,
        EUR: 59.9,
        SEK: 599,
      },
      field: '24',
      packageType: signPackages.TWENTY_FIVE_SIGNATURES,
      tooltip: `<p><b>${_l`24 signatures`}</b></p>`
    },
    {
      packageName: _l`50 signatures`,
      packageId: {
        USD: 'price_1IZ6BAC7cXVm3yoBbnFF3FEP',
        EUR: 'price_1IZ6BAC7cXVm3yoB8PGJF5Ib',
        SEK: 'price_1IZ6B9C7cXVm3yoBIZuGnNzK' 
      },
      price: {
        USD: 109.9,
        EUR: 109.9,
        SEK: 1099,
      },
      field: '50',
      packageType: signPackages.FIFTY_SIGNATURES,
      tooltip: `<p><b>${_l`50 signatures`}</b></p>`
    },
    {
      packageName: _l`100 signatures`,
      packageId: {
        USD: 'price_1IZ6BAC7cXVm3yoBQbNGelbN',
        EUR: 'price_1IZ6BAC7cXVm3yoBda1ZrDVX',
        SEK: 'price_1IZ6B9C7cXVm3yoBP9pyeWWu' 
      },
      price: {
        USD: 199.9,
        EUR: 199.9,
        SEK: 1999,
      },
      field: '100',
      packageType: signPackages.HUNDERED_SIGNATURES,
      tooltip: `<p><b>${_l`100 signatures`}</b></p>`
    },
  ];
  const getCardType = (name) => {
    switch (name) {
      case 'visa':
        return 'VISA';
        break;
      case 'mastercard':
        return 'MASTERCARD';
        break;
      case 'american-express':
        return 'AMERICAN_EXPRESS';
        break;
      case 'discover':
        return 'DISCOVER';
        break;
    }
    return 'NONE';
  };
  const selectAddOn = (addOn) => {
    const newSelected = {...addOnSelecteds};
    const newExtra = { ...__extraPackage };
    console.log("the chosen one is here -> ", addOnSelecteds)
    if(_.size(addOnSelecteds)===0){
      if (!newSelected[addOn.field]) {
        newSelected[addOn.field] = {
          packageId: addOn.packageId[currency],
          packageList: addOn.packageId,
          priceList: addOn.price,
          price: addOn.price[currency],
        };
      }
    }
    // console.log("check type here", parseInt(Object.keys(newSelected)[0]), "some", addOn.field)
    if (parseInt(addOn.field) === parseInt(Object.keys(newSelected)[0]) && _.size(addOnSelecteds)!==0) {
      delete newSelected[addOn.field];
      delete newExtra[addOn.field]
    }
    setAddOneSelecteds(newSelected);
    setExtraPackage(newExtra);
  }
  useEffect(()=> {
    let added = 0
    for(var key in addOnSelecteds){
      added += parseInt(key)
    }
    setTotalAddSignature(added)
  }, [addOnSelecteds])

  const getPaymentInfo = async () => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/payment/getAllPaymentInfo`,
      });
      if (res) {
        let nextBillingDate = new Date();
        let discount = 0;
        res.subscriptionLiteDTOList.forEach((subscription) => {
          if (subscription.source === 'STRIPE') {
            nextBillingDate = subscription.nextBillingDate;
            discount = subscription.discountPercent;
          }
        });
        setInvoiceDate(nextBillingDate);
        setDiscountPercent(discount);
        setPendingCancel(res.pendingCancel);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getCardInfo = async () => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/payment/card/get`,
      });
      if (res)
        setCard({
          ...res,
          numberInit: res.number,
          number: undefined,
          expiredMonth: res.expiredMonth == 0 ? undefined : res.expiredMonth,
          expiredYear: res.expiredYear == 0 ? undefined : res.expiredYear,
        });
    } catch (error) {
      console.log(error);
    }
  };
  const getCurrentQuantitySignatures = async () => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/payment/getNumberSignature`
      })
      setCurrentQuantity(res)
    } catch (error) {
      console.log(error)
    }
  }
  const isEurope = (country) => {
    const europe = [
      'Austria',
      'Belgium',
      'Bulgaria',
      'Croatia',
      'Cyprus',
      'Czech Republic',
      'Denmark',
      'Estonia',
      'Finland',
      'France',
      'Sweden',
      'Germany',
      'Greece',
      'Hungary',
      'Ireland',
      'Italy',
      'Lithuania',
      'Luxembourg',
      'Latvia',
      'Malta',
      'Netherlands',
      'Poland',
      'Portugal',
      'Slovenia',
      'Slovakia',
      'Sweden',
      'Spain',
      'United Kingdom',
      'Romania',
    ];
    return _.includes(europe, country);
  };
  
  const validation = () => {
    const error = {};
    let isValid = true;
    const totalQuatity = currentQuantity + totalAddSignature;
    if (totalAddSignature === 0 || totalQuatity === 0) {
      isValid = false;
      error.numberMoreLicenseZero = true;
    }
    if (!card.numberInit && !card.number) {
      isValid = false;
      error.cardNumberRequired = true;
    }
    if (card.number && !valid.number(card.number).isValid) {
      isValid = false;
      error.cardNumberRequired = true;
    }

    if (!card.holderName) {
      isValid = false;
      error.nameCardRequired = true;
    }
    if (!card.numberInit && !card.cscCode) {
      isValid = false;
      error.codeCardRequired = true;
    }
    if (!card.type) {
      isValid = false;
      error.cardNumberInvalid = true;
    }
    setError(error);
    return isValid;
  };

  const subscribe = () => {
    if (validation()) {
      console.log('Process Payment');
      // processPayment();
      processCardInfo();
    } else {
      console.log('errror');
      setSubmittingSubscribe(false);
      setShowModal(false);
    }
  }

  const [errorCardInfo, setErrorCardInfo] = useState(null);

  const processCardInfo = async () => {
    const dto = {
      vatNumber: null,
      vatPercent: vatPercent,
      currency: currency,
      numberNewPayingSignature: 10
    };
    try {
      const res = await api.post({
        resource: `${Endpoints.Enterprise}/payment/stripe/purchaseESignature`,
        data: dto,
      });
      if (res) {
        // const popup = popupWindow('https://invoice.stripe.com/i/acct_150CklC7cXVm3yoB/invst_IRqtd9azzbie2tgT4lN5g23JHtOPxjE', 'Stripe', 600, 800);

        if (res === 'SUCCESS') {
          setErrorCardInfo(null);
          setShowModal(true);
          setConnectWithStripe(true);
          // setAddOneSelecteds({});
          // setExtraPackage({});
          setTimeout(() => {
            getPaymentInfo();
          }, 10000);
        } else {
          const popup = popupWindow(res, 'Stripe', 600, 800);
          let timer = setInterval(() => {
            if (!popup || popup.closed) {
              clearInterval(timer);
            }
          }, 100);
        }
      }
    } catch (error) {
      console.log(error);
      if (error?.errorMessage === 'STRIPE_ERROR') {
        setErrorCardInfo(error?.additionInfo);
        setSubmittingSubscribe(false);
        return;
      }
      if (error.message) {
        if (error.message == 'CURRENT_SUBSCRIPTION_IS_CANCELLED') {
          props.putError(
            _l`You already have an active subscription that has been canceled. You can activate it again on ${moment(
              invoiceDate
            ).format('DD MMM YYYY')}`,
            null,
            null,
            true
          );
        } else {
          props.putError(error.message);
        }
      }
      console.log('e');
    }
    setSubmittingSubscribe(false);
  };


  const populateVat = (company) => {
    if (!company) return;
    let vatNumberCorrect = false;
    const vatNumber = company.vat !== null ? company.vat : '';
    const country = company.country;
    let vatValue = 0;
    const europe = isEurope(country);
    if (europe && vatNumber.length > 0) {
      const countryCode = vatNumber.substring(0, 2);
      const checkVatResult = checkVAT(vatNumber, countries);
      if (countryCode === 'SE') {
        vatValue = 25;
        if (checkVatResult.isValid && checkVatResult.country && _.lowerCase(checkVatResult.country.name) === _.lowerCase(country)) {
          vatNumberCorrect = true;
        }
        setVatPercent(vatValue);
      } else {
        if (checkVatResult.isValid && checkVatResult.country && _.lowerCase(checkVatResult.country.name) === _.lowerCase(country)) {
          vatNumberCorrect = true;
          vatValue = 0;
        } else {
          vatValue = 25;
        }
        setVatPercent(vatValue);
      }
    } else if (europe && vatNumber.length === 0) {
      vatValue = 25;
      setVatPercent(vatValue);
    } else {
      vatValue = 0;
      setVatPercent(0);
    }
  };

  const populateDefaultCurrencyByCountry = (country) => {
    if (!country || country.length === 0) {
      setCurrency('USD');
      return;
    }
    if (SEKCountries.includes(country)) {
      setCurrency('SEK');
      return;
    }
    if (EURCountries.includes(country)) {
      setCurrency('EUR');
      return;
    }
    setCurrency('USD');
  };

  const populateDefaultCurrency = (currency, country) => {
    if (currency === 'NO_FIXED_CURRENCY') {
      populateDefaultCurrencyByCountry(country);
      return;
    }
    setCurrency(currency);
  };

  const getCompanyInfo = async () => {
    try {
      const res = await api.get({
        resource: `${Endpoints.Enterprise}/company/get`,
      });
      if (res) {
        populateVat(res);
        if (res.stripeCustomerId) {
          setConnectWithStripe(true);
        }
      }
      const billinCurrency = await api.get({
        resource: `${Endpoints.Enterprise}/payment/getBillingCurrency`,
      });
      if (billinCurrency) {
        populateDefaultCurrency(billinCurrency, res.country);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <Grid columns={3}>
        <Grid.Row>
          <Grid.Column>
            <BillingPane step={1} title={_l`Licenses`}>
              <Licenses
                __extraPackage={__extraPackage}
                extraPackages={signPackages}
                listAddOn={listAddOn}
                currency={currency}
                addOnSelecteds={addOnSelecteds}
                selectAddOn={selectAddOn}
                signature={true}
              />
            </BillingPane>
          </Grid.Column>
          <Grid.Column>
            <BillingPane step={2} title={_l`Card info`}>
              <Card
                card={card}
                error={error}
                setCard={setCard}
                setError={setError}
                getCardType={getCardType}
                getPaymentInfo={getPaymentInfo}
                isConnectWithStripe={isConnectWithStripe}
                submitting={submitting}
                setSubmitting={setSubmitting}
              />
            </BillingPane>
          </Grid.Column>
          <Grid.Column>
            <BillingPane step={3} title={_l`Billing info`}>
              <BillingInfo
                invoiceDate={invoiceDate}
                currency={currency}
                currentQuantity={currentQuantity}
                // numberMoreLicense={_.size(addOnSelecteds)}
                numberMoreLicense={totalAddSignature}
                totalQuatity={totalQuatity}
                subTotal={subTotal}
                discountPercent={discountPercent}
                vatPercent={vatPercent}
                total={total}
                totalQuatityMessege={totalQuatityMessege}
                isConnectWithStripe={isConnectWithStripe}
                subscribe={subscribe}
                coupon={coupon}
                setCoupon={setCoupon}
                error={error}
                // subscribeMaestrano={subscribeMaestrano}
                // maestranoUserId={props.maestranoUserId}
                // initData={initData}
                pendingCancel={pendingCancel}
                submitting={submittingSubscribe}
                setSubmitting={setSubmittingSubscribe}
                setDiscountPercent={setDiscountPercent}
                errorCardInfo={errorCardInfo}
                unitPrice={0}
                period={'Month'}
                signature={true}
              />
            </BillingPane>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <BillingModal
        visible={showModal}
        onClose={() => setShowModal(false)}
        totalQuatity={totalQuatity}
        currency={currency}
        vatPercent={vatPercent}
        discountPercent={discountPercent}
        total={total}
        invoiceDate={invoiceDate}
        signature={true}
      />
    </>
  )
};

export default connect(
  (state) => {
    return {
      token: state.auth.token,
    }
  },
  {
    putSuccess: NotificationActions.success,
    putError: NotificationActions.error
  }
)(ESignatureComponent)