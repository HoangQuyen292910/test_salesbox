/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState } from 'react';
import cx from 'classnames';
import { Popup, Icon } from 'semantic-ui-react';
import _l from 'lib/i18n';
import css from './licenses.css';
import otherCss from '../../../CompanySettings/DefaultValues/SaleProcess/SaleProcess.css';
import { iconMoney } from '../../../../Constants';

const Licenses = ({
  listAddOn,
  currency,
  period,
  addOnSelecteds,
  __extraPackage,
  numberPaidLicense,
  numberMoreLicense,
  removeNumberLicense,
  addNumberLicense,
  selectAddOn,
  signature
}) => {
  const customParseFloat = (number) => {
    if (isNaN(parseFloat(number)) === false) {
      let toFixedLength = 0;
      const str = String(number);

      ['.', ','].forEach((seperator) => {
        const arr = str.split(seperator);
        if (arr.length === 2) {
          toFixedLength = 2;
        }
      });
      return parseFloat(str).toFixed(toFixedLength);
    }
    return number;
  };

  return (
    <>
    { !signature ?
      <div className={css.numberLice}>
        <a className={css.remove} onClick={removeNumberLicense}>-</a>
        <span>{numberPaidLicense + numberMoreLicense}</span>
        <a className={css.add} onClick={addNumberLicense}>+</a>
      </div> : 
      <div style={{textAlign: 'center'}}>
      {/* <Icon id="userMdId" name='file alternate outline' size='huge'/> */}
      <img style={{width: 40}} src={require('../../../../../public/doc-status-normal.svg')} />
    </div>
      }
      <div style={{ marginTop: signature ? '15px' : '18px', marginRight: '5px' }}>
        <div className={cx(css.listAddOne, css.Title)}>
          <div className={css.addOne}>
            {signature ? <span>{_l`Package`}</span> : <span>{_l`Add-on`}</span>}
          </div>
          <div className={css.activated}>
            
            {signature ? <span>{_l`Selected`}</span> : <span>{_l`Activated`}</span>}
          </div>
          <div className={css.activated} style={{whiteSpace: 'nowrap'}}>
          {signature ? <span>{_l`Price per package`}</span> : <span>{_l`Price / user`}</span>}
          </div>
        </div>
        {listAddOn.map((addOne, index) => {
          return (
            <div className={cx(css.listAddOne)} key={index}>
              <div className={css.addOne}>
                <Popup
                  style={{ fontSize: '11px' }}
                  content={<div dangerouslySetInnerHTML={{ __html: addOne.tooltip }} />}
                  position={'left center'}
                  trigger={<span>{addOne.packageName}</span>}
                />
              </div>
              <div className={css.activated}>
                <div
                  onClick={() => selectAddOn(addOne)}
                  className={
                    __extraPackage[addOne?.field]
                      ? otherCss.setDone
                      : addOnSelecteds[addOne?.field]
                        ? otherCss.notSetasDone
                        : otherCss.btn
                  }
                >
                  <div />
                </div>
              </div>
              { !signature ?
                <div className={css.activated}>
                  <strong>
                    {customParseFloat(addOne.price[currency])} {`${iconMoney[currency]} / ${_l.call(this, [period])}`}
                  </strong>
                </div> :
                <div className={css.activated}>
                  <strong>
                    {customParseFloat(addOne.price[currency])} {`${iconMoney[currency]}`}
                  </strong>
                </div>
              }
            </div>
          );
        })}
      </div>
    </>
  );
};
export default Licenses;
