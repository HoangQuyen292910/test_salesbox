import makeAsyncComponent from 'lib/makeAsyncComponent';

const Subscriptions = makeAsyncComponent(() => import('./subscriptions'));
const Services = makeAsyncComponent(() => import('./services'));
const SalesAcademy = makeAsyncComponent(() => import('./salesAcademy'));
const ESignature = makeAsyncComponent(()=> import('./eSignature'))
export { Subscriptions, Services, SalesAcademy, ESignature };
